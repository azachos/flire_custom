#-------------------------------------------------------------------------------
# Purpose:     Configuration parameter for the FLIRE project
# Author:      George Karavokiros
# Created:     07/12/2013
# Copyright:   (c) George Karavokiros 2013
# Licence:     Free software
#-------------------------------------------------------------------------------
from datetime import datetime, timedelta

starttime = datetime.now() # A starting time of the process
##starttime = datetime(starttime.year, starttime.month, starttime.day, 00, 00)
#starttime = datetime(2013, 11, 24, 00, 05) # Date of heavy reainfall (for demo) - Measured: Pikermi 33.8mm
#starttime = datetime(2013, 02, 16, 00, 00) # Date of heavy reainfall (for demo) - Measured: Pikermi 32mm
#HECBASEDIR      = "I:/CHI/FLIRE/prj/HEC/"
#starttime = datetime(2014, 9, 06, 00, 00)

HECBASEDIR      = "C:/"
PRGBASE         = "C:/Program Files (x86)/"
DSSVUE          = "%sHEC/HEC-DSSVue/HEC-DSSVue.exe"%(PRGBASE)
HECHMS          = "%sHEC/HEC-HMS/3.5/HEC-HMS.cmd"%(PRGBASE)
SWMM            = "%sEPA SWMM 5.1/swmm5.exe"%(PRGBASE)

###22-2-2013 event
##starttime = datetime(2013, 02, 21, 00, 00)
##startsim = datetime(2013, 02, 21, 00, 00)
##endsim = datetime(2013, 02, 23, 23, 50)

WEATHERFORECASTURLBASE = 'http://139.91.194.18/flire/weatherforecastdata/'

##OFFSETDAYS             = 0 # Download precipitation data OFFSETDAYS before today
##DAYSPREDICTION         = 1 # Number of days in total to be downloaded from the prediction files
##SIMDURATION            = 1 # Duration of the simulation period
##startsim = starttime - timedelta(days=OFFSETDAYS) # Starting of simulation is OFFSETDAYS days before today
##endsim   = startsim + timedelta(days=SIMDURATION) # End of the simulation period

#Parameters controlling coordinates transformation
DOCOORDTRANFORM = True   # True: Do coordinate transformation
INITEPSG        = "2100" # The original coordinate system is given in this code
TARGETEPSG      = "4326" # The target coordinate system for presentation or results

#SWMM directory
SWMMDIR         = "%sSWMM/" % HECBASEDIR
# HEC Files and Directories
HMSDIR          = 'HEC_projects/HMS/'
RASDIR          = 'HEC_projects/RAS/'
SCRIPTS         = '%sscripts/' % HECBASEDIR
RAIN            = '%s%srain/' % (HECBASEDIR,HMSDIR)
BATCHFILE       = 'flire.bat'
HMS_NAME        = "flire"
STATIONS        = "forecast.txt"
GAGE_NAME       = "flire"
MET_NAME        = "flire"
ACCESS_NAME     = "flire"
CONTROL_NAME    = "flire"
DSS_NAME        = "flire"
BASIN_NAME      = "flire"
RUN_NAME        = "flire"
DSSFILE         = '%s%s%s.dss'%(HECBASEDIR,HMSDIR,DSS_NAME)
DSCFILE         = '%s%s%s.dsc'%(HECBASEDIR,HMSDIR,DSS_NAME)
SHAPEFILES      = "Subbasin977"
SHAPEFILES      = '%s%smaps/%s'%(HECBASEDIR,HMSDIR,SHAPEFILES)
HMSOUTPUTDIR    = '%s%s'%(HECBASEDIR,HMSDIR)
BASIN_FILE_PART2= "basin_part_2.basin"
BASIN_FILE_PART3= "basin_part_3.basin"
DSSVUE_PUT_TS   = "DSSVue_put_ts.py"
DSSVUE_EXPORT_TS= "DSSVue_export_ts.py" # Script for exporting flow data from the DSS-file in form of a HTML-file using HEC-DSSVue
DSSVUE_FLOW_HTML= "DSSVue_flow.html" # The HTML file exported from the DSS#
DSSVUE_FLOW_XML = "DSSVue_flow.xml" # The XML file exported from the DSS#
HMS_RUN         = "HMS_run.bat"
KMLFILE         = "flire_flood_inundation.kml"
SWMM_KML        = "swmm_polygons.kml"
SWMM_SHP        = "swmm_shapefiles/Surface_Pond_Polygon"
EVENT_RAIN      = "%s/2013year.xls" % HECBASEDIR
EVENT_TAB       = "14November"

# Regular expressions for parsing XML aerial precipitation files
re_point       = '<point>(?P<pointValue>.+)</point>'
re_date        = '<date>(?P<dateValue>\s*\d{8,8}\s*)</date>'
re_time        = '<time>(?P<timeValue>\s*\d{1,2}\s*)</time>'
re_data        = '<data>(?P<dataValue>.+)</data>'
re_hourly_rain = '<hourly_rain>(?P<hourly_rainValue>.+)</hourly_rain>'
re_value       = '<value>(?P<paramValue>\s*\d*\.?\d+\s*)</value>'
re_coordinates = '<coordinates>(?P<coordinatesValue>.+)</coordinates>'
re_x           = '<x>(?P<xValue>\s*\d*\.?\d+\s*)</x>'
re_y           = '<y>(?P<yValue>\s*\d*\.?\d+\s*)</y>'
# Regular expressions for parsing XML flow data
re_CaptionRows   = '<CaptionRow>(?P<CaptionRows>(\n|.)*)</CaptionRow>'
re_CaptionItem    = '^\s*<CaptionItem>(?P<CaptionItem>.+)</CaptionItem>'
re_ValuesRows   = '<ValuesRow>(?P<ValuesRows>(\n|.)*)</ValuesRow>'
re_ValuesItem    = '^\s*<ValuesItem>(?P<ValuesItem>.+)</ValuesItem>'



#Regions to be calculated by RAS {region:[subbasins]}
regions = [
('Spata', 'W17990'),
]

# Parameters of the .basin file, needed for HMS
subbasins = {
'subbasin-1' :[
     ('Canvas X'               , 482408.524994709),
     ('Canvas Y'               , 4207946.343132275),
     ('Area'                   , 8.9),
     ('Downstream'             , 'Junction-1'),
     ('Canopy'                 , 'None'),
     ('Surface'                , 'None'),
     ('LossRate'               , 'SCS'),
     ('Percent Impervious Area', 0.0),
     ('Curve Number'           , 70),
     ('Initial Abstraction'    , 5),
     ('Transform'              , 'Snyder'),
     ('Snyder Method'          , 'Standard'),
     ('SnyderTp'               , 0.4),
     ('SnyderCp'               , 0.4),
     ('Baseflow'               , 'None'),],
'W17990' :[
     ('Canvas X'               , 487353.6158),
     ('Canvas Y'               , 4205231.4082),
     ('Label X'                , 16.0),
     ('Label Y'                , 16.0),
     ('Area'                   , 23.683675),
     ('Downstream'             , 'Spata'),
     ('Canopy'                 , 'None'),
     ('Surface'                , 'None'),
     ('LossRate'               , 'SCS'),
     ('Percent Impervious Area', 0.0),
     ('Curve Number'           , 53),
     ('Initial Abstraction'    , 12),
     ('Transform'              , 'Snyder'),
     ('Snyder Method'          , 'Standard'),
     ('SnyderTp'               , 0.7),
     ('SnyderCp'               , 0.4),
     ('Baseflow'               , 'Recession'),
     ('Recession Factor'       , 0.7),
     ('Initial Flow'           , 0.05),
     ('Threshold Flow'         , 0.2),],
'W15020' :[
     ('Canvas X'               , 492986.6827),
     ('Canvas Y'               , 4205998.572),
     ('Label X'                , 16.0),
     ('Label Y'                , 16.0),
     ('Area'                   , 68.452025),
     ('Downstream'             , 'Rafina'),
     ('Canopy'                 , 'None'),
     ('Surface'                , 'None'),
     ('LossRate'               , 'SCS'),
     ('Percent Impervious Area', 0.0),
     ('Curve Number'           , 58),
     ('Initial Abstraction'    , 13),
     ('Transform'              , 'Snyder'),
     ('Snyder Method'          , 'Standard'),
     ('SnyderTp'               , 0.8),
     ('SnyderCp'               , 0.4),
     ('Baseflow'               , 'Recession'),
     ('Recession Factor'       , 0.7),
     ('Initial Flow'           , 0.5),
     ('Threshold Flow'         , 0.5),],
'W15460' :[
     ('Canvas X'               , 492067.2457),
     ('Canvas Y'               , 4210441.9861),
     ('Label X'                , 16.0),
     ('Label Y'                , 16.0),
     ('Area'                   , 17.540275),
     ('Observed Hydrograph Gage', 'Drafi'),
     ('Downstream'             , 'Drafi'),
     ('Canopy'                 , 'None'),
     ('Surface'                , 'None'),
     ('LossRate'               , 'SCS'),
     ('Percent Impervious Area', 0.0),
     ('Curve Number'           , 50),
     ('Initial Abstraction'    , 11),
     ('Transform'              , 'Snyder'),
     ('Snyder Method'          , 'Standard'),
     ('SnyderTp'               , 0.5),
     ('SnyderCp'               , 0.4),
     ('Baseflow'               , 'Recession'),
     ('Recession Factor'       , 0.8),
     ('Initial Flow'           , 0.3),
     ('Flow to Peak Ratio'     , 0.1),],
'W16340' :[
     ('Canvas X'               , 497185.8169),
     ('Canvas Y'               , 4209073.9091),
     ('Label X'                , 16.0),
     ('Label Y'                , 16.0),
     ('Area'                   , 11.4364),
     ('Downstream'             , 'Rafina2'),
     ('Canopy'                 , 'None'),
     ('Surface'                , 'None'),
     ('LossRate'               , 'SCS'),
     ('Percent Impervious Area', 0.0),
     ('Curve Number'           , 55),
     ('Initial Abstraction'    , 10),
     ('Transform'              , 'Snyder'),
     ('Snyder Method'          , 'Standard'),
     ('SnyderTp'               , 0.7),
     ('SnyderCp'               , 0.4),
     ('Baseflow'               , 'Recession'),
     ('Recession Factor'       , 0.7),
     ('Initial Flow'           , 0.3),
     ('Threshold Flow'         , 0.3),],
'W16890' :[
     ('Canvas X'               , 499505.1285),
     ('Canvas Y'               , 4205970.609666667),
     ('Label X'                , 16.0),
     ('Label Y'                , 16.0),
     ('Area'                   , 1.9049),
     ('Downstream'             , 'Outlet5'),
     ('Canopy'                 , 'None'),
     ('Surface'                , 'None'),
     ('LossRate'               , 'SCS'),
     ('Percent Impervious Area', 0.0),
     ('Curve Number'           , 65),
     ('Initial Abstraction'    , 7),
     ('Transform'              , 'Snyder'),
     ('Snyder Method'          , 'Standard'),
     ('SnyderTp'               , 0.3),
     ('SnyderCp'               , 0.4),
     ('Baseflow'               , 'Recession'),
     ('Recession Factor'       , 0.6),
     ('Initial Flow'           , 0.8),
     ('Flow to Peak Ratio'     , 0.01),],
}

reaches = {
'Shraga' :[
    ('Canvas X'                 , 486269.66256084654),
    ('Canvas Y'                 , 4206600.25847619),
    ('From Canvas X'            , 484002.57261375664),
    ('From Canvas Y'            , 4206777.374878307),
    ('Downstream'               , 'Junction-2'),
    ('Route'                    , 'Muskingum'),
    ('Muskingum K'              , 0.4),
    ('Muskingum X'              , 0.4),
    ('Muskingum Steps'          , 2),
    ('Channel Loss'             , 'None'),],
'Reach5' :[
    ('Canvas X'                 , 490223.816),
    ('Canvas Y'                 , 4203706.568),
    ('From Canvas X'            , 486269.66256084654),
    ('From Canvas Y'            , 4206600.25847619),
    ('Downstream'               , 'Spata'),
    ('Route'                    , 'Muskingum'),
    ('Muskingum K'              , 0.7),
    ('Muskingum X'              , 0.25),
    ('Muskingum Steps'          , 5),
    ('Channel Loss'             , 'None'),],
'Reach1' :[
    ('Canvas X'                 , 497983.816),
    ('Canvas Y'                 , 4206931.568),
    ('From Canvas X'            , 493483.816),
    ('From Canvas Y'            , 4207771.568),
    ('Downstream'               , 'Rafina'),
    ('Route'                    , 'Muskingum'),
    ('Muskingum K'              , 0.5),
    ('Muskingum X'              , 0.25),
    ('Muskingum Steps'          , 3),
    ('Channel Loss'             , 'None'),],
'Reach2' :[
    ('Canvas X'                 , 497983.816),
    ('Canvas Y'                 , 4206931.568),
    ('From Canvas X'            , 490223.816),
    ('From Canvas Y'            , 4203706.568),
    ('Downstream'               , 'Rafina'),
    ('Route'                    , 'Muskingum'),
    ('Muskingum K'              , 0.8),
    ('Muskingum X'              , 0.2),
    ('Muskingum Steps'          , 6),
    ('Channel Loss'             , 'None'),],
'Reach3' :[
    ('Canvas X'                 , 500173.816),
    ('Canvas Y'                 , 4207511.568),
    ('From Canvas X'            , 497983.816),
    ('From Canvas Y'            , 4206931.568),
    ('Downstream'               , 'Rafina2'),
    ('Route'                    , 'Muskingum'),
    ('Muskingum K'              , 0.5),
    ('Muskingum X'              , 0.25),
    ('Muskingum Steps'          , 3),
    ('Channel Loss'             , 'None'),],
'Reach4' :[
    ('Canvas X'                 , 501081.316),
    ('Canvas Y'                 , 4207576.568),
    ('From Canvas X'            , 500173.816),
    ('From Canvas Y'            , 4207511.568),
    ('Downstream'               , 'Outlet5'),
    ('Route'                    , 'Muskingum'),
    ('Muskingum K'              , 0.2),
    ('Muskingum X'              , 0.2),
    ('Muskingum Steps'          , 1),
    ('Channel Loss'             , 'None'),],
}


##Spata	W17990/FLOW +REACH5/FLOW-IN
##   400	SPATA/FLOW
##Drafi	W15460/FLOW
##   466.6289	DRAFI/FLOW
##Rafina
##Reach Drafi 1	REACH1/FLOW-IN+(1/3*W15020/FLOW)
##   195.9763	RAFINA/FLOW
##Reach Rafina 1	REACH2/FLOW-IN+(2/3*W15020/FLOW)
##Rafina2	REACH3/FLOW-IN+W16340/FLOW
##   41.5085	RAFINA2/FLOW
##Urban	REACH4/FLOW-IN+W16890/FLOW
##   163.4048	OUTLET5/FLOW


maxflows = {
'DRAFI/FLOW'     : -9999,
'OUTLET5/FLOW'   : -9999,
'RAFINA/FLOW'    : -9999,
'RAFINA2/FLOW'   : -9999,
'REACH1/FLOW-IN' : -9999,
'REACH2/FLOW-IN' : -9999,
'REACH3/FLOW-IN' : -9999,
'REACH4/FLOW-IN' : -9999,
'REACH5/FLOW-IN' : -9999,
'SPATA/FLOW'     : -9999,
'W15020/FLOW'    : -9999,
'W15020/FLOW'    : -9999,
'W15460/FLOW'    : -9999,
'W16340/FLOW'    : -9999,
'W16890/FLOW'    : -9999,
'W17990/FLOW'    : -9999,
}

reaches_RAS = {
'Spata' : {
  'maxflows' : [
['9201.298', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['8801.901', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['8401.717', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['8001.354', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['7332.477', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['6800.731', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['6400.692', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['6001.155', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['5600.863', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['5200.367', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['4574.179', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['4000.389', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['3600.391', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['3200.359', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['2800.354', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['2571.913', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['2171.773', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['1600.327', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['1355.757', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['800.551', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['400.991', 	 "maxflows['W17990/FLOW'] + maxflows['REACH5/FLOW-IN']"],
['0.096309', 	 "maxflows['SPATA/FLOW']"],
    ],
  'minelevation' : {}, # Minimum channel elevation at a specific cross section identifier
  'water_surface': {}, # Water surface elevation at a specific cross section identifier
  'water_depth'  : {}, # Min. water depth calculated by HEC-RAS at a specific cross section identifier
  'cross_section': {}, # Geometry of the cross section at a specific cross section identifier as specified in the .gxx file {cross section 1: {'index': X, 'points': [(distance, elevation), (...,...)]},}
  'cs_cut_line'  : {}, # Pair of coordinates defining the cut line of each cross section {cross section 1: (X1,Y1,X2,Y2),}
  'flood_area'   : [], # Polygon defining the flood inundation area
  },
'Drafi' :{
  'maxflows' : [
['8044.295'  , "maxflows['W15460/FLOW']"],
['7618.356'  , "maxflows['W15460/FLOW']"],
['7200'  , "maxflows['W15460/FLOW']"],
['6800'  , "maxflows['W15460/FLOW']"],
['6400'  , "maxflows['W15460/FLOW']"],
['5987.358'  , "maxflows['W15460/FLOW']"],
['5600'  , "maxflows['W15460/FLOW']"],
['5200'  , "maxflows['W15460/FLOW']"],
['4679.895'  , "maxflows['W15460/FLOW']"],
['4323.258'  , "maxflows['W15460/FLOW']"],
['4040.972'  , "maxflows['W15460/FLOW']"],
['3600'  , "maxflows['W15460/FLOW']"],
['3200'  , "maxflows['W15460/FLOW']"],
['2718.591'  , "maxflows['W15460/FLOW']"],
['2324.026'  , "maxflows['W15460/FLOW']"],
['2000'  , "maxflows['W15460/FLOW']"],
['1692.663'  , "maxflows['W15460/FLOW']"],
['1240.636'  , "maxflows['W15460/FLOW']"],
['799.9999'  , "maxflows['W15460/FLOW']"],
['469.8896'  , "maxflows['W15460/FLOW']"],
['5.3422'  , "maxflows['W15460/FLOW']"],
    ],
  'minelevation' : {}, # Minimum channel elevation at a specific cross section identifier
  'water_surface': {}, # Water surface elevation at a specific cross section identifier
  'water_depth'  : {}, # Min. water depth calculated by HEC-RAS at a specific cross section identifier
  'cross_section': {}, # Geometry of the cross section at a specific cross section identifier as specified in the .gxx file {cross section 1: {'index': X, 'points': [(distance, elevation), (...,...)]},}
  'cs_cut_line'  : {}, # Pair of coordinates defining the cut line of each cross section {cross section 1: (X1,Y1,X2,Y2),}
  'flood_area'   : [], # Polygon defining the flood inundation area
  },
'Rafina_Drafi' : {
  'maxflows' : [
['5574.119'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['5200'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['5000'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['4800'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['4600'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['4400'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['4200'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['3800'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['3600'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['3400'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['3200'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['3000'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['2800'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['2600'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['2400'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['2200'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['1800'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['1600'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['1400'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['1200'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['999.9999'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['800'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['600'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['401.972'  , "maxflows['REACH1/FLOW-IN'] + 1/3.0 * maxflows['W15020/FLOW']"],
['5.843997'  , "maxflows['RAFINA/FLOW']"],
    ],
  'minelevation' : {}, # Minimum channel elevation at a specific cross section identifier
  'water_surface': {}, # Water surface elevation at a specific cross section identifier
  'water_depth'  : {}, # Min. water depth calculated by HEC-RAS at a specific cross section identifier
  'cross_section': {}, # Geometry of the cross section at a specific cross section identifier as specified in the .gxx file {cross section 1: {'index': X, 'points': [(distance, elevation), (...,...)]},}
  'cs_cut_line'  : {}, # Pair of coordinates defining the cut line of each cross section {cross section 1: (X1,Y1,X2,Y2),}
  'flood_area'   : [], # Polygon defining the flood inundation area
  },
'Rafina' : {
  'maxflows' : [
['11014.97'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['10600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['10400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['10200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['10000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['9800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['9600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['9400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['9200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['9000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['8800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['8600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['8400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['7600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['7400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['7000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['6800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['6600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['6400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['6200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['6000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['5600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['5400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['5200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['5000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['4800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['4600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['4400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['4200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['4000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['3800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['3600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['3400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['3200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['3000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['2800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['2600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['2400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['2200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['2000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['1800'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['1600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['1400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['1200'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['1000'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['800.0001'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['600'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['400'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['199.9999'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
['46.14413'  , "maxflows['REACH2/FLOW-IN'] + 2/3.0 * maxflows['W15020/FLOW']"],
    ],
  'minelevation' : {}, # Minimum channel elevation at a specific cross section identifier
  'water_surface': {}, # Water surface elevation at a specific cross section identifier
  'water_depth'  : {}, # Min. water depth calculated by HEC-RAS at a specific cross section identifier
  'cross_section': {}, # Geometry of the cross section at a specific cross section identifier as specified in the .gxx file {cross section 1: {'index': X, 'points': [(distance, elevation), (...,...)]},}
  'cs_cut_line'  : {}, # Pair of coordinates defining the cut line of each cross section {cross section 1: (X1,Y1,X2,Y2),}
  'flood_area'   : [], # Polygon defining the flood inundation area
  },
'Rafina2' : {
  'maxflows' : [
['2776.76'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['2457.695'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['2200'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['2000'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['1800'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['1600'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['1379.695'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['1200'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['1000'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['600'  , "maxflows['REACH3/FLOW-IN'] + maxflows['W16340/FLOW']"],
['0.366904'  , "maxflows['RAFINA2/FLOW']"],
    ],
  'minelevation' : {}, # Minimum channel elevation at a specific cross section identifier
  'water_surface': {}, # Water surface elevation at a specific cross section identifier
  'water_depth'  : {}, # Min. water depth calculated by HEC-RAS at a specific cross section identifier
  'cross_section': {}, # Geometry of the cross section at a specific cross section identifier as specified in the .gxx file {cross section 1: {'index': X, 'points': [(distance, elevation), (...,...)]},}
  'cs_cut_line'  : {}, # Pair of coordinates defining the cut line of each cross section {cross section 1: (X1,Y1,X2,Y2),}
  'flood_area'   : [], # Polygon defining the flood inundation area
  },
'Urban' : {
  'maxflows' : [
['996.4858'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['831.803'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['710.951'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['599.9999'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['419.4989'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['299.9999'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['209.795'  , "maxflows['REACH4/FLOW-IN'] + maxflows['W16890/FLOW']"],
['79.64833'  , "maxflows['OUTLET5/FLOW']"],
    ],
  'minelevation' : {}, # Minimum channel elevation at a specific cross section identifier
  'water_surface': {}, # Water surface elevation at a specific cross section identifier
  'water_depth'  : {}, # Min. water depth calculated by HEC-RAS at a specific cross section identifier
  'cross_section': {}, # Geometry of the cross section at a specific cross section identifier as specified in the .gxx file {cross section 1: {'index': X, 'points': [(distance, elevation), (...,...)]},}
  'cs_cut_line'  : {}, # Pair of coordinates defining the cut line of each cross section {cross section 1: (X1,Y1,X2,Y2),}
  'flood_area'   : [], # Polygon defining the flood inundation area
  },
}

# Reaches included in RAS subbasins (used to create flows (.f) files)
# [(region name,(reach 1, reach 2, ...)]
regions_RAS = [
                ('Spata',   {'Spata'        : {'Up Slope': 0.09,
                                               'Dn Slope': 0.015,},}),
                ('Drafi',   {'Drafi'        : {'Up Slope': 0.33,
                                               'Dn Slope': 0.13,},}),
                ('Rafina',  {'Rafina_Drafi' : {'Up Slope': 0.029,
                                               'Dn Slope': 0.005,},
                             'Rafina'       : {'Up Slope': 0.047,
                                               'Dn Slope': 0.015,},}),
                ('Rafina2', {'Rafina2'      : {'Up Slope': 0.019,
                                               'Dn Slope': 0.01,},}),
                ('Urban',   {'Urban'        : {'Up Slope': 0.079,
                                               'Dn Slope': 0.0001,},}),
]

