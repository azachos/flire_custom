#-------------------------------------------------------------------------------
# Purpose:     Main module implementing the Flood Risk Assessment System
#              for the FLIRE project. Controls the dataflow from the collection
#              of areal precipitation data to the creation of flood inundation
#              maps invoking a hydrological rainfall-runoff model (e.g. HEC-HMS)
#              and an urban flood model (e.g. HEC-RAS)
# Author:      George Karavokiros
# Created:     06/12/2013
# Copyright:   (c) George Karavokiros 2013
# Licence:     Free software
#-------------------------------------------------------------------------------
import urllib
import re
import codecs
from datetime import datetime, timedelta
import os
import subprocess
import shutil
import shapefile # pyshp
import pprint
import xlrd,xlwt

from flire_config import *
from flire_hms import create_HEC_HMS_files, export_flow_from_DSS, export_flow_from_DSS_custom
from flire_ras import run_ras, create_flow_file, loadCrossSections, loadMinElevation, calcMinElevation, calcWaterDepth, calcFloodInundation, createKML,createKMLsimple
from flire_auxiliary import point_inside_polygon, point_inside_any_polygon
from flire_SWMM import readSWMMCoords, readSWMMresults, writeSWMMrain,shapeSWMMreader

global starttime
global startsim
global endsim

def parsePrecipitationXML(point_data, URL, shapeRecs):
    filename = re.sub('\W', '_', URL)
    # If does not exist, download the file first
    if not os.path.exists("%s%s"%(RAIN,filename)):
        with codecs.open("%s%s"%(RAIN,filename), encoding='utf-8', mode='w') as f:
            f.write(urllib.urlopen(URL).read())
    # Read precipitation data from file from the cache
    with codecs.open("%s%s"%(RAIN,filename), encoding='utf-8', mode='r') as f:
        xml = f.read()

    matchObjPoint = re.search(re_point, xml)
    i_all = 0
    i_inside = 0
    while matchObjPoint:
        matchObjCoordinates = re.search(re_coordinates, matchObjPoint.group('pointValue'))
        if matchObjCoordinates:
            i_all += 1
            matchObjX = re.search(re_x, matchObjCoordinates.group('coordinatesValue'))
            matchObjY = re.search(re_y, matchObjCoordinates.group('coordinatesValue'))
            x_val    = float(matchObjX.group('xValue'))
            y_val    = float(matchObjY.group('yValue'))
            # print i_all, x_val, y_val
            # Ignore points which are located outside any subbasin of the study area
            if point_inside_any_polygon(x_val, y_val, shapeRecs):
                matchObjDate = re.search(re_date, matchObjPoint.group('pointValue'))
                matchObjTime = re.search(re_time, matchObjPoint.group('pointValue'))
                matchObjData = re.search(re_data, matchObjPoint.group('pointValue'))
                if matchObjData:
                    matchObjHourly_rain = re.search(re_hourly_rain, matchObjData.group('dataValue'))
                    if matchObjHourly_rain:
                        matchObjHourly_rain_val = re.search(re_value, matchObjHourly_rain.group('hourly_rainValue'))
                #Check if dataset is complete
                if matchObjDate and matchObjTime and matchObjHourly_rain_val and matchObjX and matchObjY:
                    date_val = matchObjDate.group('dateValue')
                    time_val = int(matchObjTime.group('timeValue'))
                    rain_val = float(matchObjHourly_rain_val.group('paramValue'))
                    x_val    = float(matchObjX.group('xValue'))
                    y_val    = float(matchObjY.group('yValue'))
                    if (x_val, y_val) in point_data:
                          point_data[(x_val, y_val)] += [(date_val, time_val, rain_val)]
                    else: point_data[(x_val, y_val)] =  [(date_val, time_val, rain_val)]
                    i_inside += 1
        xml = xml[matchObjPoint.start()+4:] #+4 and not matchObjPoint in order not to miss the next <Point>
        matchObjPoint =  re.search(re_point, xml)
    print '  %s  -  All points:%d  -  inside study area:%d'%(URL, i_all, i_inside)

    Dstr = ''
    for point in point_data:
        Dstr = Dstr + '%s - %s'%(point,point_data[point])

    with codecs.open(STATIONS, encoding="utf-8", mode="w") as f_dss:
        f_dss.write(Dstr)

def parseFlowXML():
    ''' Reads the flow file DSSVUE_FLOW_XML and returns a dictionary of items
        each having an ordered values list. The first two items are not flows but ordinates and dates.'''
    flows = {}
    order_flows = []
    with codecs.open('%sscripts/%s'%(HECBASEDIR, DSSVUE_FLOW_XML), encoding='utf-8', mode='r') as f_flow:
        xml_lines = f_flow.read()
  # Get the captions
    matchObjCaptionItems = re.search(re_CaptionRows, xml_lines)
    if matchObjCaptionItems:
        xml = matchObjCaptionItems.group('CaptionRows')
        matchObjCaptionItem = re.search(re_CaptionItem, xml)
        while matchObjCaptionItem:
            caption = matchObjCaptionItem.group('CaptionItem')
            #print caption
            captionsplit = caption.split()
            flowpath = '%s'%(captionsplit[0])
            if len(captionsplit)>1: flowpath += '/%s'%(caption.split()[1])
            flows[flowpath] = []
            order_flows += [flowpath]
            xml = xml[matchObjCaptionItem.end():]
            matchObjCaptionItem =  re.search(re_CaptionItem, xml)
  # Create dictionary keys
    #print flows # Emplty lists
  # Get the values
    xml_lines = xml_lines[matchObjCaptionItems.end():]
    matchObjValuesItems = re.search(re_ValuesRows, xml_lines)
    while matchObjValuesItems:
        xml = matchObjValuesItems.group('ValuesRows')
        matchObjValuesItem = re.search(re_ValuesItem, xml)
        iItem = 0
        while matchObjValuesItem:
            value =  matchObjValuesItem.group('ValuesItem')
            #print value
            flows[order_flows[iItem]] += [value.replace(',','.')]# replace decimal comma with point
            xml = xml[matchObjValuesItem.end():]
            matchObjValuesItem =  re.search(re_ValuesItem, xml)
            iItem += 1
        xml_lines = xml_lines[matchObjValuesItems.start()+4:] #+4 in order not to miss the next <ValuesRow>
        matchObjValuesItems =  re.search(re_ValuesRows, xml_lines)
    #print flows #  {item name : [list of values]}
    return flows



# This function implements a linear disaggregation and returnhs the time series in form of a list (timestamp, value)
def disaggregate_ts(hr_ts):
    ts = []
    for i in range(0, len(hr_ts)-1):
        #print hr_ts[i], hr_ts[i+1]
        incr = (hr_ts[i+1][2]-hr_ts[i][2])/6
        for j in range (0,6):
            val = hr_ts[i][2] + j*incr
            year = hr_ts[i][0][0:4]
            month = hr_ts[i][0][4:6]
            day = hr_ts[i][0][6:8]
            hour = hr_ts[i][1]
            #print year, month, day, hour
            timestamp = datetime(int(year), int(month), int(day), int(hour), 0, 0) + timedelta(minutes=30 + 10*j)
            ts.append((timestamp, val))
    #print ts
    return ts


def createHMSScripts():
    ''' Creates scripts getHMS.py and HMS_run.py to be used by HEC-HMS'''
# #####    getHMS.py    ###########
    with codecs.open("getHMS.py", encoding='utf-8', mode='w') as f:
            f.write('''# Script for extract resulting discharges from DSS to txt-files using HEC-DSSVue

from hec.script import *
from hec.heclib.dss import *
from hec.heclib.util import *
from hec.io import *
import java
import codecs

try :
    FlireDSS = HecDss.open("%s")

    flow = HecDss.get("//W17990/FLOW/08DEC2013/10MIN/RUN:RUN 1/")
    AStr = ""
    with codecs.open("test.txt", encoding="utf-8", mode="w") as f_flow:
        f_flow.write(AStr)

finally :
    FlireDSS.done()'''%DSSFILE)

# #####    HMS_run.py    ###########
    with codecs.open("HMS_run.py", encoding='utf-8', mode='w') as f:
            f.write('''from hms.model.JythonHms import *

OpenProject("flire", "%s%s")
Compute ("Run 1") # A simulation run with this code has to be present
Exit (1)'''% (HECBASEDIR,HMSDIR))

# #####    HMS_run.bat    ###########
    with codecs.open("HMS_run.bat", encoding='utf-8', mode='w') as f:
            f.write('''c:
cd "%sHEC/HEC-HMS/3.5"
"%s" -s "%sHMS_run.py"'''% (PRGBASE,HECHMS,SCRIPTS))

# #####    DSSVue_export_ts.bat    ###########
    with codecs.open("DSSVue_export_ts.bat", encoding='utf-8', mode='w') as f:
            f.write('''c:
cd "%sHEC/HEC-HMS/3.5"
"%s" "%sDSSVue_export_ts.py"'''% (PRGBASE,DSSVUE,SCRIPTS))

#merge two dictionaries of lists
def merge_dols(dol1, dol2):
  keys = set(dol1).union(dol2)
  no = []
  return dict((k, dol1.get(k, no) + dol2.get(k, no)) for k in keys)


def main():

    #read custom timeseries of events
    workbook = xlrd.open_workbook(EVENT_RAIN)
    #event_stations = workbook.sheet_names()

    worksheet = workbook.sheet_by_name(EVENT_TAB)
    #worksheet = workbook.sheet_by_index(0)
    global starttime
    global startsim
    global endsim

    num_rows = worksheet.nrows-1
    start_date = xlrd.xldate_as_tuple(worksheet.cell_value(1,0),workbook.datemode)
    start_time = xlrd.xldate_as_tuple(worksheet.cell_value(1,1),workbook.datemode)
    end_date = xlrd.xldate_as_tuple(worksheet.cell_value(num_rows,0),workbook.datemode)
    end_time = xlrd.xldate_as_tuple(worksheet.cell_value(num_rows,1),workbook.datemode)
    starttime = datetime(start_date[0], start_date[1], start_date[2], start_time[3], start_time[4])
    startsim = starttime
    endsim   = datetime(end_date[0], end_date[1], end_date[2], end_time[3], end_time[4])
    DAYSPREDICTION = endsim - startsim
    DAYSPREDICTION = DAYSPREDICTION.days
    print startsim
    print endsim
    print DAYSPREDICTION
    print EVENT_TAB
    #exit()
    # worksheet = workbook.sheet_by_name(event) #load event sheet
    # Create HMS_run.py file
    print "Creating HMS scripts..."
    createHMSScripts()
    # Read the shapefile of the study area
    print "Reading shapefiles..."
    #sf = shapefile.Reader("one_subbasin/sub1") # Shapefile
    sf = shapefile.Reader(SHAPEFILES) # Shapefile
    shapes = sf.shapes()
    shapeRecs = sf.shapeRecords()
    print "  Number of shapes: %d"%len(shapeRecs)
    ##
    ##  # Download the predictions
    ##    print "Downloading prediction files..."
    ##    point_data = {}  # {(X,Y):[(date,hour,value)]}
    ##  # For DAYSPREDICTION days from the starting day and for every hour of the day download the data (if available)
    ##    for i in range (0,DAYSPREDICTION):
    ##        cur_date = startsim + timedelta(days=i)
    ##        for hour in range (0,24):
    ##            curr_year = cur_date.strftime("%Y")
    ##            curr_month = cur_date.strftime("%m")
    ##            curr_day = cur_date.strftime("%d")
    ##            parsePrecipitationXML(point_data, '%s%s%s/%s/%s%s%s%s.xml'%(WEATHERFORECASTURLBASE, curr_year,curr_month,curr_day, curr_year,curr_month,curr_day,"%02d"%hour), shapeRecs)
    ##
    ##
    ##  # Statistics
    ##    print "Statistics..."
    ##    for shapeRec in shapeRecs:
    ##        i = 0
    ##        for point in point_data:
    ##            if point_inside_polygon(point[0],point[1],shapeRec.shape.points):
    ##                i += 1
    ##        print "  Shape %s:  Number of polygon points: %d  -  Number of gridpoints within polygon: %d"%(shapeRec.record[10], len(shapeRec.shape.points), i) # Field record[10] is the code of the subcatc
    ##        if i==0: print '  WARNING: No gridpoint within the polygon'
    ##
    ##
    ##  # Disaggregate the time series from 1h time step to 10min time step
    ##    print "Disaggregating timeseries..."
    ##    point_data_10min =  {}  # {(X,Y):[(date,hour,value)]}
    ##    for point in point_data:
    ##        point_data_10min[point] = disaggregate_ts(point_data[point])
    ##        if point == (493793.21,4205640.61): #store nearest grid point to pikermi rainfall for SWMM input
    ##            pikermi_rain =  point_data_10min[point]
    ##
    # Read timeseries of past event
    point_data_10min = {}

    j = 0
    for i in range(2,8):
        curr_row = 0
        num_rows = worksheet.nrows-1
        while curr_row < num_rows:
            curr_row += 1
            x_val = 0
            y_val = j
            date = xlrd.xldate_as_tuple(worksheet.cell_value(curr_row,0),workbook.datemode)
            time = xlrd.xldate_as_tuple(worksheet.cell_value(curr_row,1),workbook.datemode)
            #date_val = datetime.strptime(worksheet.cell_value(curr_row,0),  '%H:%M')
            #timestamp = datetime(starttime.year, starttime.month, starttime.day, date_val.hour, date_val.minute, 0)
            timestamp = datetime(date[0], date[1], date[2], time[3], time[4], 0)
            rain_val = worksheet.cell_value(curr_row,i)
            if (x_val,y_val) in point_data_10min:
               point_data_10min[(x_val,y_val)] += [(timestamp, rain_val)]
            else:
               point_data_10min[(x_val,y_val)] =  [(timestamp, rain_val)]
        j += 1


    pikermi_rain = point_data_10min[(0,3)]
    # Create the HEC-HMS input files
    print "Creating HEC-HMS project..."
    create_HEC_HMS_files(point_data_10min, startsim, endsim, shapeRecs)

    # Run HEC-HMS
    print "Running HEC-HMS simulation..."
    #subprocess.call('"%s\\%s"'%(os.path.dirname(os.path.realpath(__file__)),HMS_RUN))
    #subprocess.call('"C:\\Program Files\\HEC\\HEC-HMS\\3.5\\HEC-HMS.cmd" -s "I:\\CHI\\FLIRE\\prj\\HEC\\scripts\\runHMS.py"') # this is not working
    #subprocess.call("%s",HMS_RUN)
    subprocess.call('C:\scripts\HMS_run.bat')
    # Prepare the HEC-RAS scenario
    # Export all flow time series from DSS file to a file
    print "Exporting simulation results..."
    # export_flow_from_DSS(maxflows) INITIAL CODE
    export_flow_from_DSS_custom(maxflows,-1,startsim)
    flows = parseFlowXML()
    # Export all day flows
    for i in range(DAYSPREDICTION):
        export_flow_from_DSS_custom(maxflows, i, startsim)
        flows_temp = parseFlowXML()
        flows = merge_dols(flows, flows_temp)

    # Read the file and create a dictionary of flow time series and identify flow peak
    print "Identifying flow peaks..."
    #  flows = parseFlowXML() INITIAL CODE
    for flowpath in maxflows:
        maxflows[flowpath] = max([float(flow) for flow in flows[flowpath]])
    # Create the RAS files
    print "Creating and running HEC-RAS projects..."
    for r in regions_RAS:
        print '  Creating files for region: %s'%r[0]
        create_flow_file(r, startsim, endsim)
        print '  Simulating region: %s'%r[0]
    # Run HEC-RAS and extract the results
        run_ras(r)
    # Load cross-section geometry for every reach and calculate min. elevation
    print "Loading geometry and calculating min. elevation..."
    calcMinElevation()
    ##    loadCrossSections()
    ##  # Load minimum channel elevation for every reach from files
    ##    loadMinElevation()
    # Calculate water depth for every cross-section and every reach
    print "Calculating water depths..."
    calcWaterDepth()
    # Print max. water depths (informative only)
    for reach in reaches_RAS:
        maxdepth = -9999
        csID = ''
        for  section in reaches_RAS[reach]['water_depth']:
            if maxdepth<reaches_RAS[reach]['water_depth'][section]:
                maxdepth = reaches_RAS[reach]['water_depth'][section]
                csID = section
        print '  Max water depth at reach %s (cross-section:%s): %f'%(reach,csID,maxdepth)

    ##  # Load cross-section geometry for every reach
    ##    print "Loading geometry..."
    ##    loadCrossSections()
    # Calculate flood inundation area for every reach
    print "Calculatuing flood area..."
    calcFloodInundation()

    # Execute SWMM
    # Create input file for SWMM
    # Run SWMM
    print "Running SWMM simulation..."
    #subprocess.call([SWMM, "%s19.3.2014a - 2013.02.20 00.00.inp"%SWMMDIR, "%stemp.rpt"%SWMMDIR, "%stemp.out"%SWMMDIR])
    # writeSWMMrain("%stest.inp"%SWMMDIR, pikermi_rain, flows.get('RAFINA/FLOW','none'))
    # subprocess.call([SWMM, "%stest.inp"%SWMMDIR, "%sfinal.rpt"%SWMMDIR, "%sfinal.out"%SWMMDIR])
    # Read node coordinates
    #nodes = readSWMMCoords("%s19.3.2014a - 2013.02.20 00.00.inp"%SWMMDIR)
    nodes = readSWMMCoords("%sfinal2.inp"%SWMMDIR)
    # Exctract flood (water depth) information for the urban area
    #nodes = readSWMMresults("%s19.3.2014a - 2013.02.20 00.00 - Copy.rpt"%SWMMDIR, nodes)
    #nodes = readSWMMresults("%sfinal.rpt"%SWMMDIR, nodes)
    # Creating KML file of the flood inundation area
    print "Creating KML file of the flood inundation area..."
    pols = shapeSWMMreader()
    scenario = 0
    createKMLsimple(nodes, pols, scenario, startsim)
    #os.remove("%sfinal.out"%SWMMDIR)
    #os.remove("%sfinal.rpt"%SWMMDIR)



##Notes for FORTH:
## The URL of the catalogues of data start with http://139.91.194.18/flire/EN/, whereas the URL of the actual data start with http://139.91.194.18/flire/
## Appeard once: IOError: [Errno socket error] [Errno 10060] A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond



if __name__ == '__main__':
    startcycle = datetime.now()

##  # Read the file and create a dictionary of flow time series and identify flow peak
##    print "Identifying flow peaks..."
##    flows = parseFlowXML()
##    for flowpath in maxflows:
##        maxflows[flowpath] = max([float(flow) for flow in flows[flowpath]])
##    #print maxflows
##  # Create the RAS files
##    print "Creating and running HEC-RAS projects..."
##    for r in regions_RAS:
##        print '  Creating files for region: %s'%r[0]
##        create_flow_file(r)
##        print '  Simulating region: %s'%r[0]
##  # Run HEC-RAS and extract the results
##        run_ras(r)
##  # Load cross-section geometry for every reach and calculate min. elevation
##    print "Loading geometry and calculating min. elevation..."
##    calcMinElevation()
####    loadCrossSections()
####  # Load minimum channel elevation for every reach from files
####    print "Calculating water depths..."
####    loadMinElevation()
##  # Calculate water depth for every cross-section and every reach
##    calcWaterDepth()
##  # Print max. water depths (informative only)
##    for reach in reaches_RAS:
##        maxdepth = -9999
##        csID = ''
##        for  section in reaches_RAS[reach]['water_depth']:
##            if maxdepth<reaches_RAS[reach]['water_depth'][section]:
##                maxdepth = reaches_RAS[reach]['water_depth'][section]
##                csID = section
##        print '  Max water depth at reach %s (cross-section:%s): %f'%(reach,csID,maxdepth)
##
####  # Load cross-section geometry for every reach
####    print "Loading geometry..."
####    loadCrossSections()
##  # Calculate flood inundation area for every reach
##  #   print "Calculatuing flood area..."
##  #  calcFloodInundation()
##  # Creating KML file of the flood inundation area
##  #  print "Creating KML file of the flood inundation area..."
##  #  createKML()
##
##
##    print "Testing..."
##   ## nodes = readSWMMCoords("%s19.3.2014a - 2013.02.20 00.00.inp"%SWMMDIR)
##   ## readSWMMresults("%s19.3.2014a - 2013.02.20 00.00 - Copy.rpt"%SWMMDIR, nodes)
##   ## print nodes

    main()

    print 'Done! Elapsed time:%s'%(datetime.now()-startcycle)

