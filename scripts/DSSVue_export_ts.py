from hec.heclib.dss import * # for DSS
from hec.script import * # for Tabulate

try :
    try :
        dssfile = HecDss.open("C:/HEC_projects/HMS/flire.dss") # open the file
        theTable = Tabulate.newTable() # create the table
        theTable.setTitle("Flows") # set the table title
        flow_0 = dssfile.get("//REACH5/FLOW-IN/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_0) # add the data
        flow_1 = dssfile.get("//REACH2/FLOW-IN/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_1) # add the data
        flow_2 = dssfile.get("//W15020/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_2) # add the data
        flow_3 = dssfile.get("//W16340/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_3) # add the data
        flow_4 = dssfile.get("//REACH4/FLOW-IN/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_4) # add the data
        flow_5 = dssfile.get("//W17990/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_5) # add the data
        flow_6 = dssfile.get("//W15460/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_6) # add the data
        flow_7 = dssfile.get("//DRAFI/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_7) # add the data
        flow_8 = dssfile.get("//REACH3/FLOW-IN/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_8) # add the data
        flow_9 = dssfile.get("//RAFINA2/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_9) # add the data
        flow_10 = dssfile.get("//OUTLET5/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_10) # add the data
        flow_11 = dssfile.get("//RAFINA/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_11) # add the data
        flow_12 = dssfile.get("//SPATA/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_12) # add the data
        flow_13 = dssfile.get("//REACH1/FLOW-IN/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_13) # add the data
        flow_14 = dssfile.get("//W16890/FLOW/05NOV2013/10MIN/RUN:RUN 1/")
        theTable.addData(flow_14) # add the data
        theTable.exportAsXML("C:/scripts/DSSVue_flow.xml")
        theTable.close()
    except :
        print "Exception raised"
finally :
    dssfile.done()
