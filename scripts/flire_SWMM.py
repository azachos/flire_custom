#-------------------------------------------------------------------------------
# Name:        flire_SWMM.py
# Purpose:     Processes related to the SWMM model for the FLIRE project
# Author:      George Karavokiros
# Created:     06/06/2014
# Copyright:   (c) george 2014
# Licence:     Free software
#-------------------------------------------------------------------------------
import codecs
import shapefile
from flire_config import *
from datetime import datetime, timedelta
import xml.etree.ElementTree as ET


def readSWMMCoords(inpfile):
    """ Reads all node coordinates from the given SWMM input file (.inp)
        and returns a dictionary of nodes with their coordinates."""
    nodes = {}
    with codecs.open(inpfile, mode="r") as f:
        lines = f.readlines()
        for (i,line) in enumerate(lines):
            if '[COORDINATES]' in line: break
        for iline in range(i+3,len(lines)):
            line = lines[iline]
            if not line.strip(): break
            if line[0]==';': continue
            nodes[line[0:17].strip()] = {'X':float(line[17:34]),'Y':float(line[34:]),}
    return nodes

def writeSWMMrain(inpfile, rain_serie, flow_serie):
    """ Writes Pikermi Rainfall forecast to the SWMM input file (.inp)"""
    print "Writing Pikermi Rainfall and Rafina runoff to .inp file"
    AStr = '\n\n[TIMESERIES]\n'
    AStr = AStr + ';;Name           Date       Time       Value\n'
    AStr = AStr + ';;-------------- ---------- ---------- ----------\n'
    AStr = AStr + ';Rainfall (mm/hr)\n'
    BAStr = '\n\n;Flow (m?/s)\n'
    start = rain_serie[0][0]
    print len(rain_serie)
    print len(flow_serie)
    start_sim = startsim
    for val in rain_serie:
        AStr = AStr + '%02d-%02d/%02d/%04d %02d/%02d/%04d %02d:%02d:00    %.1f\n' %(startsim.day, endsim.day, startsim.month, startsim.year, val[0].month,val[0].day,val[0].year,val[0].hour,val[0].minute,val[1])
    for i in range(len(flow_serie)):
        BAStr = BAStr + 'runoffRafina     %02d/%02d/%04d %02d:%02d:00   %.2f\n' %(start_sim.month, start_sim.day, start_sim.year, start_sim.hour, start_sim.minute, float(flow_serie[i]))
        start_sim += timedelta(minutes=10)
    AStr = AStr + BAStr
    # Read the first part from input file final_1
    with codecs.open("%sfinal_first.inp"%SWMMDIR, encoding='utf-8', mode='r') as f_final_part1:
        part1 = f_final_part1.read()
        part1 = part1.replace('<start-date>','%02d/%02d/%04d'%(startsim.month,startsim.day,startsim.year))
        part1 = part1.replace('<end-date>','%02d/%02d/%04d'%(endsim.month,endsim.day,endsim.year))
        part1 = part1.replace('<start-date-rain>','%02d-%02d/%02d/%04d'%(startsim.day,endsim.day,startsim.month,startsim.year))
        FStr = part1 + AStr
    # Read the second part from input file final_2
    with codecs.open("%sfinal_second.inp"%SWMMDIR, encoding='utf-8', mode='r') as f_final_part2:
        FStr = FStr + f_final_part2.read()
    with codecs.open(inpfile, encoding='utf-8',mode="w") as f:
        f.write(FStr)



def readSWMMresults(rptfile, nodes):
    """ Extracts water depth informationfrom the given SWMM report file (.rpt)
        for the given nodes as calculated by SWMM."""
    with codecs.open(rptfile, mode="r") as f:
        lines = f.readlines()
        for (i,line) in enumerate(lines):
            if '  Node Depth Summary'== line[0:20]: break
        for iline in range(i+8,len(lines)):
            line = lines[iline]
            if not line.strip(): break
            if line[0]==';': continue
            if line[0:17].strip() not in nodes: print 'Error: Node "%s" without coordinates'%line[0:17].strip()
            else: nodes[line[0:17].strip()]['depth'] = float(line[40:49])
    return nodes

def shapeSWMMreader():
##    swmm_kml = ET.parse(SWMM_KML)
##    for point in swmm_kml.findall('Name'):
##        print point
    print "Reading shapefiles..."
    #sf = shapefile.Reader("one_subbasin/sub1") # Shapefile
    sf = shapefile.Reader(SWMM_SHP) # Shapefile
    shapes = sf.shapes()
##    print sf.fields
##    print len(shapes)
##    for name in dir(shapes[3]):
##        if not name.startswith('__'):
##            print name
##
##    points = shapes[3].points
##    for i in points:
##        print '(%.3f,%.3f)'%(i[0],i[1])
    return shapes
    #shapeRecs = sf.shapeRecords()
    #print "  Number of shapes: %d"%len(shapeRecs)
