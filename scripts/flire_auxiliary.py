#-------------------------------------------------------------------------------
# Purpose:     Auxiliary routines for the FLIRE project
# Author:      George Karavokiros
# Created:     07/12/2013
# Copyright:   (c) George Karavokiros 2013
# Licence:     Free software
#-------------------------------------------------------------------------------


known_locations = {}

# Determine if a point is inside a given polygon or not
# Polygon is a list of (x,y) pairs.
# (Ray Casting Algorithm)
def point_inside_polygon(x,y,poly):
    n = len(poly)
    inside =False
    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y
    return inside


#Checks if one of the given polygons (subcatchments) contains the location given by its coordinates and in this case returns True.
#For performance reasons it maintains a directory of known (already checked) locations and checks there first.
def point_inside_any_polygon(X,Y,shapeRecs):
    if (X,Y) in known_locations: # Location is known?
        return len(known_locations[X,Y])>0 # =0 means location is outside any polygon, >0: location is at least within one polygon
    for shapeRec in shapeRecs:
        if point_inside_polygon(X,Y,shapeRec.shape.points):
            if (X,Y) in known_locations:
                  known_locations[X,Y] += [shapeRec.record[10]]
            else: known_locations[X,Y] = [shapeRec.record[10]]
            return True
    known_locations[X,Y] = []
    return False



def monthToStrDSS(month, uppercase=False):
    if   month==1:
        if uppercase: return 'JAN'
        else: return 'Jan'
    elif month==2:
        if uppercase: return 'FEB'
        else: return 'Feb'
    elif month==3:
        if uppercase: return 'MAR'
        else: return 'Mar'
    elif month==4:
        if uppercase: return 'APR'
        else: return 'Apr'
    elif month==5:
        if uppercase: return 'MAY'
        else: return 'May'
    elif month==6:
        if uppercase: return 'JUN'
        else: return 'Jun'
    elif month==7:
        if uppercase: return 'JUL'
        else: return 'Jul'
    elif month==8:
        if uppercase: return 'AUG'
        else: return 'Aug'
    elif month==9:
        if uppercase: return 'SEP'
        else: return 'Sep'
    elif month==10:
        if uppercase: return 'OCT'
        else: return 'Oct'
    elif month==11:
        if uppercase: return 'NOV'
        else: return 'Nov'
    elif month==12:
        if uppercase: return 'DEC'
        else: return 'Dec'
    else: return ''

def monthToStrLong(month):
    if   month==1:  return 'January'
    elif month==2:  return 'February'
    elif month==3:  return 'March'
    elif month==4:  return 'April'
    elif month==5:  return 'May'
    elif month==6:  return 'June'
    elif month==7:  return 'July'
    elif month==8:  return 'August'
    elif month==9:  return 'September'
    elif month==10: return 'October'
    elif month==11: return 'November'
    elif month==12: return 'December'
    else: return ''


def convert_SDFtoKML(sdf_file):
    with codecs.open(sdf_file, encoding="utf-8", mode="r") as f:
        sdf_lines = f.readlines()





