#-------------------------------------------------------------------------------
# Purpose:     Routines for the preparation of the HEC-HMS scenario, i.e.
#              creation of the relevant files. Developed under for the FLIRE project
# Author:      George Karavokiros
# Created:     07/12/2013
# Copyright:   (c) George Karavokiros 2013
# Licence:     Free software
#-------------------------------------------------------------------------------

from flire_config import *
from flire_auxiliary import point_inside_polygon, monthToStrDSS, monthToStrLong
import codecs
import os
import subprocess

def create_HEC_HMS_files(point_data_10min, startsim, endsim, shapeRecs):
  # Create .hms file (project definitions)
    print '  Creating the file %s.hms'%(HMS_NAME)
    AStr = ''
    AStr = AStr + 'Project: %s\n'%DSS_NAME
    AStr = AStr + '     Description:Created on %s for the FLIRE project\n'%(starttime.strftime("%A, %d. %B %Y %I:%M%p"))
    AStr = AStr + '     Version: 3.4\n'
    AStr = AStr + '     DSS File Name: %s.dss\n'%DSS_NAME
    AStr = AStr + 'End:\n\n'
    AStr = AStr + 'Precipitation: %s\n'%MET_NAME
    AStr = AStr + '     FileName: %s.met\n'%MET_NAME
    AStr = AStr + '     Description: Volume and time equally distributed\n'
    AStr = AStr + 'End:\n\n'
    AStr = AStr + 'Basin: %s\n'%BASIN_NAME
    AStr = AStr + '     FileName: %s.basin\n'%BASIN_NAME
    AStr = AStr + '     Description:\n'
    AStr = AStr + 'End:\n\n'
    AStr = AStr + 'Control: %s\n'%CONTROL_NAME
    AStr = AStr + '     FileName: %s.control\n'%CONTROL_NAME
    AStr = AStr + '     Description:\n'
    AStr = AStr + 'End:\n\n'
    with codecs.open('%s%s.hms'%(HMSOUTPUTDIR,HMS_NAME), encoding='utf-8', mode='w') as f_hms:
        f_hms.write(AStr)

  # Create .gage file (definition of gages - reference to the DSS file)
    print '  Creating the file %s.gage'%(GAGE_NAME)
    AStr = ''
    AStr = AStr + 'Gage Manager: %s\n'%GAGE_NAME
    AStr = AStr + '     Version: 3.4\n'
    AStr = AStr + 'End:\n\n'
    for idx,point in enumerate(point_data_10min):
        AStr = AStr + 'Gage: Location_%d\n'%(idx+1)
        AStr = AStr + '     Last Modified Date: %s\n'%(starttime.strftime("%Y-%m-%d"))
        AStr = AStr + '     Last Modified Time: %s\n'%(starttime.strftime("%H:%M:%S"))
        AStr = AStr + '     Units System: SI\n'
        AStr = AStr + '     Reference Height Units: Meters\n'
        AStr = AStr + '     Reference Height: 10.0\n'
        AStr = AStr + '     Gage Type: Precipitation\n'
        AStr = AStr + '     Precipitation Type: Incremental\n'
        AStr = AStr + '     Units: MM\n'
        AStr = AStr + '     Data Type: PER-CUM\n'
        AStr = AStr + '     Local to Project: YES\n'
        AStr = AStr + '     Start Time: %02d %s %04d, %02d:%02d\n'%(startsim.day, monthToStrLong(startsim.month), startsim.year, startsim.hour, startsim.minute)
        AStr = AStr + '     Start Time: %02d %s %04d, %02d:%02d\n'%(endsim.day, monthToStrLong(endsim.month), endsim.year, endsim.hour, endsim.minute)
        AStr = AStr + '     DSS File: %s.dss\n'%DSS_NAME
        AStr = AStr + '     Pathname: //Location_%d/PRECIP-INC//10MIN//\n'%(idx+1)
        AStr = AStr + 'End:\n\n'
    with codecs.open('%s%s.gage'%(HMSOUTPUTDIR,GAGE_NAME), encoding='utf-8', mode='w') as f_hms:
        f_hms.write(AStr)

##  # Create .met file (distribution of gage rainfall to subbasins)
##    print '  Creating the file %s.met'%(MET_NAME)
##    AStr = ''
##    AStr = AStr + 'Meteorology: %s\n'%MET_NAME
##    AStr = AStr + '     Description: Volume and time equally distributed\n'
##    AStr = AStr + '     Last Modified Date: %s\n'%(starttime.strftime("%Y-%m-%d"))
##    AStr = AStr + '     Last Modified Time: %s\n'%(starttime.strftime("%H:%M:%S"))
##    AStr = AStr + '     Version: 3.5\n'
##    AStr = AStr + '     Unit System: Metric\n'
##    AStr = AStr + '     Precipitation Method: Weighted Gages\n'
##    AStr = AStr + '     Short-Wave Radiation Method: None\n'
##    AStr = AStr + '     Long-Wave Radiation Method: None\n'
##    AStr = AStr + '     Snowmelt Method: None\n'
##    AStr = AStr + '     Evapotranspiration Method: No Evapotranspiration\n'
##    AStr = AStr + '     Use Basin Model: %s\n'%BASIN_NAME
##    AStr = AStr + 'End:\n\n'
##
##    for idx,point in enumerate(point_data_10min):
##        AStr = AStr + 'Gage: Location_%d\n'%(idx+1)
##        AStr = AStr + '     Type: Recording\n'
##        AStr = AStr + 'End:\n'
##
##    AStr = AStr + 'Precip Method Parameters: Weighted Gages\n'
##    AStr = AStr + '     Use HEC1 Weighting Scheme: Yes\n'
##    AStr = AStr + '     Set Missing Data to Zero: Yes\n'
##    AStr = AStr + '     Use Indexing: No\n'
##    AStr = AStr + '     Allow Depth Override: No\n'
##    AStr = AStr + 'End:\n\n'
##
##    for shapeRec in shapeRecs:
##        AStr = AStr + 'Subbasin: %s\n'%shapeRec.record[10]
##        i = 0
##        for point in point_data_10min:
##            if point_inside_polygon(point[0],point[1],shapeRec.shape.points):
##                i += 1
##        weight_point = 1.0/1
##        for idx,point in enumerate(point_data_10min):
##            if point_inside_polygon(point[0],point[1],shapeRec.shape.points):
##                AStr = AStr + '     Gage: Location_%d\n'%(idx+1)
##                AStr = AStr + '     Volume Weight: %.4f\n'%weight_point
##                AStr = AStr + '     Temporal Distribution Weight: %.4f\n'%weight_point
##        AStr = AStr + '     Begin Snow: None\n'
##        AStr = AStr + 'End:\n\n'
##    with codecs.open('%s%s.met'%(HMSOUTPUTDIR,MET_NAME), encoding='utf-8', mode='w') as f_hms:
##        f_hms.write(AStr)


  # Create .basin file (basin parameters)
    print '  Creating the file %s.basin'%(BASIN_NAME)
    AStr = ''
    AStr = AStr + 'Basin: %s\n'%BASIN_NAME
    AStr = AStr + '     Description: Basin model\n'
    AStr = AStr + '     Last Modified Date: %s\n'%(starttime.strftime("%Y-%m-%d"))
    AStr = AStr + '     Last Modified Time: %s\n'%(starttime.strftime("%H:%M:%S"))
    AStr = AStr + '     Version: 3.5\n'
    AStr = AStr + '     Filepath Separator: \\\n'
    AStr = AStr + '     Unit System: Metric\n'
    AStr = AStr + '     Grid Cell File: \n'
    AStr = AStr + '     Missing Flow To Zero: No\n'
    AStr = AStr + '     Enable Flow Ratio: No\n'
    AStr = AStr + '     Allow Blending: No\n'
    AStr = AStr + '     Compute Local Flow At Junctions: No\n'
    AStr = AStr + '     Enable Sediment Routing: No\n'
    AStr = AStr + '     Enable Quality Routing: No\n'
    AStr = AStr + 'End:\n\n'
    # Read subbasin parameters from the configuration file
    for subbasin in subbasins:
        AStr = AStr + 'Subbasin: %s\n'%subbasin
        for parameter in subbasins[subbasin]:
            AStr = AStr + '     %s: %s\n'%(parameter[0], str(parameter[1]))
        AStr = AStr + 'End:\n\n'
    # Read the remaining parameters from the auxilliary file BASIN_FILE_PART2
    with codecs.open(BASIN_FILE_PART2, encoding='utf-8', mode='r') as f_basin_2:
        AStr = AStr + f_basin_2.read()

    with codecs.open('%s%s.basin'%(HMSOUTPUTDIR,BASIN_NAME), encoding='utf-8', mode='w') as f_hms:
        f_hms.write(AStr)


  # Create .control file (Simulation period)
    print '  Creating the file %s.control'%(CONTROL_NAME)
    AStr = ''
    AStr = AStr + 'Control: %s\n'%DSS_NAME
    AStr = AStr + '     Last Modified Date: %s\n'%(starttime.strftime("%Y-%m-%d"))
    AStr = AStr + '     Last Modified Time: %s\n'%(starttime.strftime("%H:%M:%S"))
    AStr = AStr + '     Start Date: %02d %s %04d\n'%(startsim.day, monthToStrLong(startsim.month), startsim.year)
    AStr = AStr + '     Start Time: %02d:%02d\n'%(startsim.hour, startsim.minute)
    AStr = AStr + '     End Date: %02d %s %04d\n'%(endsim.day, monthToStrLong(endsim.month), endsim.year)
    AStr = AStr + '     End Time: %02d:%02d\n'%(endsim.hour, endsim.minute)
    AStr = AStr + '     Time Interval: 10\n'
    AStr = AStr + '     State Grid Write Interval: 10\n'
    AStr = AStr + 'End:\n\n'
    with codecs.open('%s%s.control'%(HMSOUTPUTDIR,CONTROL_NAME), encoding='utf-8', mode='w') as f_hms:
        f_hms.write(AStr)


  # Create the run-file
    print '  Creating the file %s.run'%(RUN_NAME)
    AStr = ''
    AStr = AStr + 'Run: Run 1\n'
    AStr = AStr + '     Default Description: Yes\n'
    AStr = AStr + '     DSS File: %s.dss\n'%DSS_NAME
    AStr = AStr + '     Basin: %s\n'%BASIN_NAME
    AStr = AStr + '     Precip: %s\n'%MET_NAME
    AStr = AStr + '     Control: %s\n'%CONTROL_NAME
    AStr = AStr + 'End:\n'
    with codecs.open('%s%s.run'%(HMSOUTPUTDIR,RUN_NAME), encoding='utf-8', mode='w') as f_run:
        f_run.write(AStr)


  # Delete old DSS/DSC files
    if os.access(DSSFILE, os.F_OK):  os.remove(DSSFILE)
    if os.access(DSCFILE, os.F_OK):  os.remove(DSCFILE) #Important!

    # Create a script for creating the DSS file using HEC-DSSVue
    # One script per station because HEC-DSSVue does not execute long scripts
    # for idx,point in enumerate(point_data_10min):
    for idx in range(6):
       # print '  Importing data from Location_%d into the DSS file %s'%(idx+1, DSSFILE)
        #print point_data_10min.get((0,idx))
        AStr = ''
        AStr = AStr + '# Script for creating the DSS file using HEC-DSSVue\n\n'
        AStr = AStr + 'from hec.script import *\n'
        AStr = AStr + 'from hec.heclib.dss import *\n'
        AStr = AStr + 'from hec.heclib.util import *\n'
        AStr = AStr + 'from hec.io import *\n'
        AStr = AStr + 'import java\n\n'

        AStr = AStr + 'try :\n'
        AStr = AStr + '    FlireDSS = HecDss.open("%s")\n\n'%DSSFILE
        AStr = AStr + '    tsc = TimeSeriesContainer()\n'
        AStr = AStr + '    tsc.fullName = "//Location_%d/PRECIP-INC//10MIN//"\n'%(idx+1)
        rec = point_data_10min[(0,idx)][0]
        AStr = AStr + '    start = HecTime("%02d%s%04d", "%02d%02d")\n'% \
             (rec[0].day, monthToStrDSS(rec[0].month), rec[0].year, rec[0].hour, rec[0].minute) # e.g. "31Dec2009", "2400"
        AStr = AStr + '    tsc.interval = 10\n'
        AStr = AStr + '    prec = %s\n'%[val[1] for val in point_data_10min[(0,idx)]]
        AStr = AStr + '    times = []\n'
        AStr = AStr + '    for value in prec :\n'
        AStr = AStr + '      times.append(start.value())\n'
        AStr = AStr + '      start.add(tsc.interval)\n'
        AStr = AStr + '    tsc.times = times\n'
        AStr = AStr + '    tsc.values = prec\n'
        AStr = AStr + '    tsc.numberValues = len(prec)\n'
        AStr = AStr + '    tsc.units = "MM"\n'
        AStr = AStr + '    tsc.type = "PER-CUM"\n'
        AStr = AStr + '    FlireDSS.put(tsc)\n\n'

        AStr = AStr + 'finally :\n'
        AStr = AStr + '    FlireDSS.done()\n'
        with codecs.open(DSSVUE_PUT_TS, encoding="utf-8", mode="w") as f_dss:
            f_dss.write(AStr)

      # Run the script for creating the DSS file using HEC-DSSVue
        #os.system('"%s\\%s"'%(os.path.dirname(os.path.realpath(__file__)),BATCHFILE)) # sub-optimal solution
        #subprocess.call('"%s" "%s%s"'%(DSSVUE,SCRIPTS,DSSVUE_PUT_TS))
        subprocess.call('C:\Program Files (x86)\HEC\HEC-DSSVue\HEC-DSSVue.exe C:\scripts\DSSVue_put_ts.py')

def export_flow_from_DSS(flowpaths, startsim):
  # Script for exporting flow data from the DSS-file in form of a HTML-file using HEC-DSSVue
    AStr = ''
    AStr = AStr + 'from hec.heclib.dss import * # for DSS\n'
    AStr = AStr + 'from hec.script import * # for Tabulate\n\n'

    AStr = AStr + 'try :\n'
    AStr = AStr + '    try :\n'
    AStr = AStr + '        dssfile = HecDss.open("%s") # open the file\n'%DSSFILE
    AStr = AStr + '        theTable = Tabulate.newTable() # create the table\n'
    AStr = AStr + '        theTable.setTitle("Flows") # set the table title\n'
    for idx,flowpath in enumerate(flowpaths):
        AStr = AStr + '        flow_%d = dssfile.get("//%s/%02d%s%04d/10MIN/RUN:RUN 1/")\n'%(idx, flowpath, startsim.day, monthToStrDSS(startsim.month, True), startsim.year)
        AStr = AStr + '        theTable.addData(flow_%d) # add the data\n'%idx
    AStr = AStr + '        theTable.exportAsXML("%s%s")\n'%(SCRIPTS,DSSVUE_FLOW_XML)
    AStr = AStr + '        theTable.exportAsHTML("%s%s")\n'%(SCRIPTS,DSSVUE_FLOW_HTML)
    AStr = AStr + '        theTable.close()\n'
    AStr = AStr + '    except :\n'
    AStr = AStr + '        print "Exception raised"\n'
    AStr = AStr + 'finally :\n'
    AStr = AStr + '    dssfile.done()\n'

    with codecs.open(DSSVUE_EXPORT_TS, encoding="utf-8", mode="w") as f_dss:
        f_dss.write(AStr)

  # Run the script
   # subprocess.call('"%s" "%s%s"'%(DSSVUE,SCRIPTS,DSSVUE_EXPORT_TS))
    subprocess.call('C:\Program Files (x86)\HEC\HEC-DSSVue\HEC-DSSVue.exe C:\scripts\DSSVue_export_ts.py')

#export flows for specific offset day
def export_flow_from_DSS_custom(flowpaths, offset, startsim):
  # Script for exporting flow data from the DSS-file in form of a HTML-file using HEC-DSSVue
    AStr = ''
    AStr = AStr + 'from hec.heclib.dss import * # for DSS\n'
    AStr = AStr + 'from hec.script import * # for Tabulate\n\n'

    AStr = AStr + 'try :\n'
    AStr = AStr + '    try :\n'
    AStr = AStr + '        dssfile = HecDss.open("%s") # open the file\n'%DSSFILE
    AStr = AStr + '        theTable = Tabulate.newTable() # create the table\n'
    AStr = AStr + '        theTable.setTitle("Flows") # set the table title\n'
    for idx,flowpath in enumerate(flowpaths):
        AStr = AStr + '        flow_%d = dssfile.get("//%s/%02d%s%04d/10MIN/RUN:RUN 1/")\n'%(idx, flowpath, startsim.day+offset , monthToStrDSS(startsim.month, True), startsim.year)
        AStr = AStr + '        theTable.addData(flow_%d) # add the data\n'%idx
    AStr = AStr + '        theTable.exportAsXML("%s%s")\n'%(SCRIPTS,DSSVUE_FLOW_XML)
    AStr = AStr + '        theTable.close()\n'
    AStr = AStr + '    except :\n'
    AStr = AStr + '        print "Exception raised"\n'
    AStr = AStr + 'finally :\n'
    AStr = AStr + '    dssfile.done()\n'

    with codecs.open(DSSVUE_EXPORT_TS, encoding="utf-8", mode="w") as f_dss:
        f_dss.write(AStr)

    #print'%s %s%s'%(DSSVUE,SCRIPTS,DSSVUE_EXPORT_TS)
    #print subprocess.call('%s %s%s'%(DSSVUE,SCRIPTS,DSSVUE_EXPORT_TS))
    #subprocess.call('"%s" "%s%s"'%(DSSVUE,SCRIPTS,DSSVUE_EXPORT_TS))
    subprocess.call('C:\Program Files (x86)\HEC\HEC-DSSVue\HEC-DSSVue.exe C:\scripts\DSSVue_export_ts.py')
