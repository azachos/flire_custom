# Script for creating the DSS file using HEC-DSSVue

from hec.script import *
from hec.heclib.dss import *
from hec.heclib.util import *
from hec.io import *
import java

try :
    FlireDSS = HecDss.open("C:/HEC_projects/HMS/flire.dss")

    tsc = TimeSeriesContainer()
    tsc.fullName = "//Location_6/PRECIP-INC//10MIN//"
    start = HecTime("06Nov2013", "0000")
    tsc.interval = 10
    prec = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.2, 1.4, 0.2, 0.0, 0.2, 0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 1.4, 0.2, 1.0, 1.0, 0.6, 0.6, 0.6, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.2, 1.0, 1.0, 1.0, 0.6, 0.6, 0.6, 0.8, 0.4, 0.2, 0.0, 0.0, 0.2, 0.0, 1.4, 4.8, 4.8, 1.2, 0.6, 0.2, 0.4, 0.4, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4, 0.4, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    times = []
    for value in prec :
      times.append(start.value())
      start.add(tsc.interval)
    tsc.times = times
    tsc.values = prec
    tsc.numberValues = len(prec)
    tsc.units = "MM"
    tsc.type = "PER-CUM"
    FlireDSS.put(tsc)

finally :
    FlireDSS.done()
