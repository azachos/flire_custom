﻿#-------------------------------------------------------------------------------
# Purpose:     Execute HEC-RAS for the FLIRE project
# Author:      George Karavokiros
# Created:     06/12/2013
# Copyright:   (c) George Karavokiros 2013
# Licence:     Free software
#-------------------------------------------------------------------------------

import win32com.client
import os
##from ctypes import*
import codecs
import re
import pyproj
import simplekml

from flire_config import *
from flire_auxiliary import monthToStrDSS

#import pythoncom

def read_TS(region):
  # Create a script for reading the DSS file using HEC-DSSVue
  # One script per station because HEC-DSSVue does not execute long scripts
    for idx,point in enumerate(point_data_10min):
        print 'Reading timeseries from the DSS file %s'%(idx+1, DSSFILE)
        AStr = ''
        AStr = AStr + '# Script for reading the DSS file using HEC-DSSVue\n\n'
        AStr = AStr + 'from hec.script import *\n'
        AStr = AStr + 'from hec.heclib.dss import *\n'
        AStr = AStr + 'from hec.heclib.util import *\n'
        AStr = AStr + 'from hec.io import *\n'
        AStr = AStr + 'import java\n\n'

        AStr = AStr + 'try :\n'
        AStr = AStr + '    FlireDSS = HecDss.open("%s")\n\n'%DSSFILE
        AStr = AStr + '    flow = HecDss.get("//%s/FLOW/%02d%s%04d/10MIN/RUN:RUN 1/")\n'%(region[0], starttime.day, monthToStrDSS(starttime.month, True), starttime.year)
        AStr = AStr + 'finally :\n'
        AStr = AStr + '    FlireDSS.done()\n'

        with codecs.open(DSS_READ_SCRIPT, encoding="utf-8", mode="w") as f_dss:
            f_dss.write(AStr)


# Loads minimum channel elevation for every reach from files
def loadMinElevation():
    for region in regions_RAS:
        for reach in region[1]:
            f = codecs.open('%s%s%s/%s.elv'%(HECBASEDIR,RASDIR,region[0],reach), encoding="utf-8", mode="r")
            try:
                lines = f.read().split('\n')
                for line in lines:
                    comp = line.split()
                    if len(comp)<>2: continue
                    reaches_RAS[reach]['minelevation'][comp[0]]= float(comp[1])
            finally:
                f.close()
                #print reaches_RAS[reach]['minelevation']



#  Min. water depth calculated by HEC-RAS comparing water elevation and min. channel elevation
def calcWaterDepth():
    for region in regions_RAS:
        f = codecs.open('%s%s%s/%s.RASexport.sdf'%(HECBASEDIR,RASDIR,region[0],region[0]), mode="r")
        try:
            lines = f.read().split('\n')
            cross_section_part = False
            for line in lines:
                if ('BEGIN CROSS-SECTIONS' in line): cross_section_part = True
                if ('END CROSS-SECTIONS' in line): cross_section_part = False
                if  cross_section_part:
                    if ('STREAM ID:' in line): # Reach
                        reach = re.search('STREAM ID:(?P<Stream>.*)', line).group('Stream').strip()
                    if ('STATION:' in line):
                        crossSectionID = re.search('STATION:(?P<Station>.*)', line).group('Station').strip()
                    if ('WATER ELEVATION:' in line):
                        elev = re.search('WATER ELEVATION:(?P<Elev>.*)', line).group('Elev').strip()
                    if ('END:' in line):
                        #print reach, crossSectionID, elev, float(elev) - float(reaches_RAS[reach]['minelevation'][station])
                        reaches_RAS[reach]['water_surface'][crossSectionID] = float(elev)
                        if reaches_RAS[reach]['minelevation'].has_key(crossSectionID):
                            reaches_RAS[reach]['water_depth'][crossSectionID] = float(elev) - float(reaches_RAS[reach]['minelevation'][crossSectionID])
                        else: print 'WARNING: No minimum elevation for cross-section: %s and region: %s'%(crossSectionID,region)
        finally:
            f.close()


# Loads cross section geometry and calculates minimum channel elevation for every reach
def calcMinElevation():
    for region in regions_RAS:
        pointidx = 0
        f = codecs.open('%s%s%s/%s.g01'%(HECBASEDIR,RASDIR,region[0],region[0]), mode="r")
        try:
            lines = f.read().split('\n')
            iLines = range(-9999, -9998) # Lines to read the next pars (distance elevation)
            iCoord = -9999 # Line to read the next coordinates of cross-section cut line
            iCS = 0
            try:
                for idx,line in enumerate(lines):
                    if ('River Reach' in line): # Reach
                        reach = re.search('River Reach=(?P<Reach>(\w|\s)*)', line).group('Reach').strip()
                    if ('Type RM Length L Ch R' in line): # Find cross-section ID
                        matchObj = re.search('Type RM Length L Ch R =\s*\d*\s*,(?P<csID>(\d|\.|\*)+)\s*,', line)
                        if matchObj:
                            csID = matchObj.group('csID').strip()
                            iCS += 1
                            iCoord = idx + 2 # Line to read the next coordinates of cross-section cut line
                            minelevation = 999999
                        else:
                            print 'WARNING: No cross-section ID could be identified in line: %s and region: %s'%(line,region[0])
                            continue
                    if idx == iCoord: # Coordinates of cross-section cut line
                        reaches_RAS[reach]['cs_cut_line'][csID] = (float(line[:16]),float(line[16:32]),float(line[32:48]),float(line[48:]))
                        #print reaches_RAS[reach]['cross_section'][csID]
                    if ('#Sta/Elev' in line): # Find number of elevation pairs
                        nrElev = int(re.search('#Sta/Elev=(?P<nrElev>.*)', line).group('nrElev').strip())
                        nrLinesElev = nrElev/5
                        if ((nrElev % 5) > 0): nrLinesElev += 1
                        iLines = range(idx+1, idx+nrLinesElev+1) # The next nrLinesElev will be read
                        pointidx += 1
                        reaches_RAS[reach]['cross_section'][csID] = {'index':pointidx,'points':[],'banks':[]}
                    if('Bank Sta' in line):
                        bank = re.search(r'Bank Sta=(.*),(.*)',line)
                        reaches_RAS[reach]['cross_section'][csID]['banks'] += (float(bank.group(1)),float(bank.group(2)))
                    if idx in iLines: # Lines of pars (distance elevation)
                        val = line.split()
                        for i in range(0, len(val), 2):
                            elev = float(val[i+1].strip())
                            reaches_RAS[reach]['cross_section'][csID]['points'] += [(float(val[i].strip()),elev)]
                            #if csID=='1514.78*': print minelevation, elev, val[i].strip()
                            if minelevation>elev: minelevation = elev
                        reaches_RAS[reach]['minelevation'][csID] = minelevation
            except: print 'ERROR reading line: %s in file: %s.g01'%(line,region[0])
        finally:
            f.close()
            print '  Geometry loaded for %d cross-sections of region %s'%(iCS, region[0])

def loadCrossSections():
    for region in regions_RAS:
        pointidx = 0
        f = codecs.open('%s%s%s/%s.g01'%(HECBASEDIR,RASDIR,region[0],region[0]), mode="r")
        try:
            lines = f.read().split('\n')
            iLines = range(-9999, -9998) # Lines to read the next pars (distance elevation)
            iCoord = -9999 # Line to read the next coordinates of cross-section cut line
            iCS = 0
            try:
                for idx,line in enumerate(lines):
                    if ('River Reach' in line): # Reach
                        reach = re.search('River Reach=(?P<Reach>(\w|\s)*)', line).group('Reach').strip()
                    if ('Type RM Length L Ch R' in line): # Find cross-section ID
                        matchObj = re.search('Type RM Length L Ch R =\s*\d*\s*,(?P< >(\d|\.|\*)+)\s*,', line)
                        if matchObj:
                            csID = matchObj.group('csID').strip()
                            iCS += 1
                            iCoord = idx + 2 # Line to read the next coordinates of cross-section cut line
                        else:
                            print 'WARNING: No cross-section ID could be identified in line: %s and region: %s'%(line,region[0])
                            continue
                    if idx == iCoord: # Coordinates of cross-section cut line
                        reaches_RAS[reach]['cs_cut_line'][csID] = (float(line[:16]),float(line[16:32]),float(line[32:48]),float(line[48:]))
                    if ('#Sta/Elev' in line): # Find number of elevation pairs
                        nrElev = int(re.search('#Sta/Elev=(?P<nrElev>.*)', line).group('nrElev').strip())
                        nrLinesElev = nrElev/5
                        if ((nrElev % 5) > 0): nrLinesElev += 1
                        iLines = range(idx+1, idx+nrLinesElev+1) # The next nrLinesElev will be read
                        pointidx += 1
                        reaches_RAS[reach]['cross_section'][csID] = {'index':pointidx,'points':[]}
                    if idx in iLines: # Lines of pars (distance elevation)
                        val = line.split()
                        for i in range(0, len(val), 2):
                            reaches_RAS[reach]['cross_section'][csID]['points'] += [(float(val[i].strip()),float(val[i+1].strip()))]
            except: print 'ERROR reading line: %s in file: %s.g01'%(line,region[0])
        finally:
            f.close()
            print '  Geometry loaded for %d cross-sections of region %s'%(iCS, region[0])


def linear_interpolation(x1,y1,x2,y2,y):
    if (y1 == y2):
        y1 += 0.05
    return x1 +((x2-x1)*(y-y1)/(y2-y1))

def calcFloodInundation():
    for reach in reaches_RAS:
        # Converts the dictionary into a sorted list ordered by the key (cross section ID). items() returns a copy of the dictionary's list of (key, value) pairs
        custom = []
        for crossSection in sorted(reaches_RAS[reach]['cross_section'].items(), key=lambda x: (x[1]['index'])): # e.g.: x[0] (sort by key),  x[1] (sort by value),  x[1]['xxx'] (sort by xxx)
            flooded = False
            passed_min_elev = False
            cs_cut_line = reaches_RAS[reach]['cs_cut_line'][crossSection[0]]
            length_cs = crossSection[1]['points'][-1][0]
            #print crossSection[1]['banks']
            end_dist_underWater = crossSection[1]['points'][-1][0] # The distance of the last point of the cross section of the dictionary values (cross section pairs)
            end_flood_area = (cs_cut_line[2],cs_cut_line[3])
          # Calculate minimum elevation for the cross-section (it has to be accurate!)
            minelev = min([dist[1] for dist in crossSection[1]['points']])
##            print minelev
##            bank = crossSection[1]['banks']
##            minelev = 9999
##            min_pos = 0 #position of min between banks
##            for dist in  crossSection[1]['points']:
##                if ((dist[1] < minelev) and (dist[0]>bank[0]) and (dist[0]<bank[1])):
##                    minelev = dist[1]
##                    minelev_sta = dist[0]
##                    min_pos += 1
##            print minelev
##            print minelev_sta
##            print min_pos
##            print crossSection[1]['banks']
##            exit()
##            bank_dep_1 = 0
##            bank_dep_2 = 0
##            for val in crossSection[1]['points']:
##                if (val[0] == crossSection[1]['banks'][0]):
##                    bank_dep_1 = val[1]
##                if (val[0] == crossSection[1]['banks'][1]):
##                    bank_dep_2 = val[1]
##            print '(%f,%f)' %(crossSection[1]['banks'][0],bank_dep_1)
##            print '(%f,%f)' %(crossSection[1]['banks'][1],bank_dep_2)


##            start = min_pos
##            points = crossSection[1]['points']
####            print points
####            exit()
##            water_surface_elev = reaches_RAS[reach]['water_surface'][crossSection[0]]
##            while (start>0):
##                if (points[start][1] > water_surface_elev):
##                    dist_underWater_prev = crossSection[1]['points'][start-1][0]
##                    elev_underWater_prev = crossSection[1]['points'][start-1][1]
##                    dist_underWater_idx = float(points[start][0])
##                    elev_underWater_idx = float(points[start][1])
##                    print dist_underWater_prev
##                    print elev_underWater_prev
##                    print dist_underWater_idx
##                    print elev_underWater_idx
##                    print water_surface_elev
##                    beg_dist_underWater = linear_interpolation(dist_underWater_prev,elev_underWater_prev,dist_underWater_idx,elev_underWater_idx,water_surface_elev)
##                    X1 = cs_cut_line[0] + (cs_cut_line[2]-cs_cut_line[0]) * (beg_dist_underWater/length_cs)
##                    Y1 = cs_cut_line[1] + (cs_cut_line[3]-cs_cut_line[1]) * (beg_dist_underWater/length_cs)
##                    beg_flood_area = (X1,Y1)
##                    break
##                start -= 1
##
##            start = min_pos
##            while (start<len(points)):
##                if (points[start][1] > water_surface_elev):
##                    dist_underWater_prev = crossSection[1]['points'][start-1][0]
##                    elev_underWater_prev = crossSection[1]['points'][start-1][1]
##                    dist_underWater_idx = float(points[start][0])
##                    elev_underWater_idx = float(points[start][1])
##                    print dist_underWater_prev
##                    print elev_underWater_prev
##                    print dist_underWater_idx
##                    print elev_underWater_idx
##                    print water_surface_elev
##                    end_dist_underWater = linear_interpolation(dist_underWater_prev,elev_underWater_prev,dist_underWater_idx,elev_underWater_idx,water_surface_elev)
##                    X2 = cs_cut_line[0] + (cs_cut_line[2]-cs_cut_line[0]) * (end_dist_underWater/length_cs)
##                    Y2 = cs_cut_line[1] + (cs_cut_line[3]-cs_cut_line[1]) * (end_dist_underWater/length_cs)
##                    end_flood_area = (X2,Y2)
##                    break
##                start += 1
##          # Identify the indexes at the cross section where flooding begins and ends
##            for idx,dist in enumerate(crossSection[1]['points']):
##              # Simulated water surface elevation at this crosss-section
##                water_surface_elev = reaches_RAS[reach]['water_surface'][crossSection[0]]
##              # Check if the minimum ground elevation (~ middle of the river bed) has been reached (in order to identify the river bed)
##                #if abs(dist[1]-reaches_RAS[reach]['minelevation'][crossSection[0]])<0.000001: # Warning: The min. elevation calculated by RAS is rounded (not accurate enough)
##                if abs(dist[1]-minelev)<0.000001:
##                    passed_min_elev = True
##              # Calculate the distance/coordinates from the beginning of the cross-section where flooding beginns
##                if dist[0] == crossSection[1]['banks'][0]: # is it under water?
##                    if idx==0: # The beginning of the cross-section is already under water
##                        beg_dist_underWater = 0
##                        beg_flood_area = (cs_cut_line[0],cs_cut_line[1])
##                    else: # calculate the distance from the side of cross section
##                        dist_underWater_prev = crossSection[1]['points'][idx-1][0]
##                        elev_underWater_prev = crossSection[1]['points'][idx-1][1]
##                        dist_underWater_idx = dist[0]
##                        elev_underWater_idx = dist[1]
##                        beg_dist_underWater = linear_interpolation(dist_underWater_prev,elev_underWater_prev,dist_underWater_idx,elev_underWater_idx,water_surface_elev)
##                        X1 = cs_cut_line[0] + (cs_cut_line[2]-cs_cut_line[0]) * (beg_dist_underWater/length_cs)
##                        Y1 = cs_cut_line[1] + (cs_cut_line[3]-cs_cut_line[1]) * (beg_dist_underWater/length_cs)
##                        beg_flood_area = (X1,Y1)
##              # Calculate the distance/coordinates from the beginning of the cross-section where flooding ends
##                elif dist[0] == crossSection[1]['banks'][1]: # was flooded --> ground surface raises above water
##                        dist_underWater_prev = crossSection[1]['points'][idx-1][0]
##                        elev_underWater_prev = crossSection[1]['points'][idx-1][1]
##                        dist_underWater_idx = dist[0]
##                        elev_underWater_idx = dist[1]
##                        end_dist_underWater = linear_interpolation(dist_underWater_prev,elev_underWater_prev,dist_underWater_idx,elev_underWater_idx,water_surface_elev)
##                        X2 = cs_cut_line[0] + (cs_cut_line[2]-cs_cut_line[0]) * (end_dist_underWater/length_cs)
##                        Y2 = cs_cut_line[1] + (cs_cut_line[3]-cs_cut_line[1]) * (end_dist_underWater/length_cs)
##                        end_flood_area = (X2,Y2)
##                        break # this was the main river bed --> break

##            custom += [(beg_flood_area, end_flood_area)]
          # Identify the indexes at the cross section where flooding begins and ends
            for idx,dist in enumerate(crossSection[1]['points']):
              # Simulated water surface elevation at this crosss-section
                water_surface_elev = reaches_RAS[reach]['water_surface'][crossSection[0]]
              # Check if the minimum ground elevation (~ middle of the river bed) has been reached (in order to identify the river bed)
                #if abs(dist[1]-reaches_RAS[reach]['minelevation'][crossSection[0]])<0.000001: # Warning: The min. elevation calculated by RAS is rounded (not accurate enough)
                if abs(dist[1]-minelev)<0.000001:
                    passed_min_elev = True
              # Calculate the distance/coordinates from the beginning of the cross-section where flooding beginns
                if dist[1]<water_surface_elev: # is it under water?
                    if flooded==False: # not flooded yet --> this is the first time under water
                        if idx==0: # The beginning of the cross-section is already under water
                            beg_dist_underWater = 0
                            beg_flood_area = (cs_cut_line[0],cs_cut_line[1])
                        else: # calculate the distance from the side of cross section
                            dist_underWater_prev = crossSection[1]['points'][idx-1][0]
                            elev_underWater_prev = crossSection[1]['points'][idx-1][1]
                            dist_underWater_idx = dist[0]
                            elev_underWater_idx = dist[1]
                            beg_dist_underWater = linear_interpolation(dist_underWater_prev,elev_underWater_prev,dist_underWater_idx,elev_underWater_idx,water_surface_elev)
                            X1 = cs_cut_line[0] + (cs_cut_line[2]-cs_cut_line[0]) * (beg_dist_underWater/length_cs)
                            Y1 = cs_cut_line[1] + (cs_cut_line[3]-cs_cut_line[1]) * (beg_dist_underWater/length_cs)
                            beg_flood_area = (X1,Y1)
                    flooded = True
              # Calculate the distance/coordinates from the beginning of the cross-section where flooding ends
                else: # ground above water surface
                    if (flooded==True) and passed_min_elev: # was flooded --> ground surface raises above water
                        dist_underWater_prev = crossSection[1]['points'][idx-1][0]
                        elev_underWater_prev = crossSection[1]['points'][idx-1][1]
                        dist_underWater_idx = dist[0]
                        elev_underWater_idx = dist[1]
                        end_dist_underWater = linear_interpolation(dist_underWater_prev,elev_underWater_prev,dist_underWater_idx,elev_underWater_idx,water_surface_elev)
                        X2 = cs_cut_line[0] + (cs_cut_line[2]-cs_cut_line[0]) * (end_dist_underWater/length_cs)
                        Y2 = cs_cut_line[1] + (cs_cut_line[3]-cs_cut_line[1]) * (end_dist_underWater/length_cs)
                        end_flood_area = (X2,Y2)
                        break # this was the main river bed --> break
                    flooded = False
            reaches_RAS[reach]['flood_area'] += [(beg_flood_area, end_flood_area)]
##            print reach, crossSection[0], beg_dist_underWater, end_dist_underWater, beg_flood_area, end_flood_area
##    print custom
##    print reaches_RAS[reach]['flood_area']
##    exit()

def createKML(nodes, shapes):
  # Do the coordinates transformation
    if DOCOORDTRANFORM:
        initEPSG   = pyproj.Proj("+init=EPSG:%s"%INITEPSG)
        targetEPSG = pyproj.Proj("+init=EPSG:%s"%TARGETEPSG)

    strKML = ''
    strKML += '<?xml version="1.0" encoding="UTF-8"?>\n'
    strKML += '<kml xmlns="http://www.opengis.net/kml/2.2">\n'
    strKML += '  <Document>\n'
    strKML += '    <Style id="transBluePoly">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>7dff0000</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>7dff0000</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly1">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff14E7FF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff14E7FF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly2">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff14CCFF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff14CCFF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly3">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff14C3FF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff14C3FF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly4">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff14B1FF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff14B1FF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly5">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff14A0FF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff14A0FF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly6">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff1485FF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff1485FF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    strKML += '    <Style id="transBluePoly7">\n'
    strKML += '      <LineStyle>\n'
    strKML += '        <width>2</width>\n'
    strKML += '        <color>ff1447FF</color>\n'
    strKML += '      </LineStyle>\n'
    strKML += '      <PolyStyle>\n'
    strKML += '        <color>ff1447FF</color>\n'
    strKML += '      </PolyStyle>\n'
    strKML += '    </Style>\n'
    count = 0
    for reach in reaches_RAS:
        strKML += '    <Placemark>\n'
        strKML += '      <name>Flood inundation area of reach %s</name>\n'%reach

##        if (count == 0):
##            strKML += '      <styleUrl>#transBluePoly</styleUrl>\n'
##        elif (count == 1):
##            strKML += '      <styleUrl>#transBluePoly1</styleUrl>\n'
##        elif (count == 2):
##            strKML += '      <styleUrl>#transBluePoly2</styleUrl>\n'
##        elif (count == 3):
##            strKML += '      <styleUrl>#transBluePoly3</styleUrl>\n'
##        elif (count == 4):
##            strKML += '      <styleUrl>#transBluePoly4</styleUrl>\n'
##        elif (count == 5):
##            strKML += '      <styleUrl>#transBluePoly5</styleUrl>\n'

        count += 1
        strKML += '      <styleUrl>#transBluePoly</styleUrl>\n'
        strKML += '      <Polygon>\n'
        strKML += '        <extrude>0</extrude>\n'
        strKML += '        <altitudeMode>relativeToGround</altitudeMode>\n'
        strKML += '        <outerBoundaryIs>\n'
        strKML += '          <LinearRing>\n'
        strKML += '            <coordinates>\n'
      # The <coordinates> for polygons must be specified in counterclockwise order
        for cs in reaches_RAS[reach]['flood_area']:
            if DOCOORDTRANFORM:
                long, lat = pyproj.transform(initEPSG, targetEPSG, cs[1][0],cs[1][1])
            else: long, lat = cs[1][0],cs[1][1]
            strKML += '              %f,%f,%d\n'%(long, lat, 10)
        for cs in reversed(reaches_RAS[reach]['flood_area']):
            if DOCOORDTRANFORM:
                long, lat = pyproj.transform(initEPSG, targetEPSG, cs[0][0],cs[0][1])
            else: long, lat = cs[0][0],cs[0][1]
            strKML += '              %f,%f,%d\n'%(long, lat, 10)
        if DOCOORDTRANFORM:
            long, lat = pyproj.transform(initEPSG, targetEPSG, reaches_RAS[reach]['flood_area'][0][1][0],reaches_RAS[reach]['flood_area'][0][1][1])
        else: long, lat = reaches_RAS[reach]['flood_area'][0][1][0],reaches_RAS[reach]['flood_area'][0][1][1]
        strKML += '              %f,%f,%d\n'%(long, lat,10) # closing the polygon
        strKML += '            </coordinates>\n'
        strKML += '          </LinearRing>\n'
        strKML += '        </outerBoundaryIs>\n'
        strKML += '      </Polygon>\n'
        strKML += '    </Placemark>\n'
    for val in nodes:
        if (nodes.get(val).get('depth') > 999): #show point if depth value is not zero
            strKML += '<Placemark>\n'
            strKML += '<name>%s' %val
            strKML += '</name>\n'
            strKML += '<description>Depth for %s is %f' %(val,nodes.get(val).get('depth'))
            strKML += '</description>\n'
            strKML += '<Point>\n'
            strKML += '<coordinates>\n'
            long, lat = pyproj.transform(initEPSG, targetEPSG, nodes.get(val).get('X'),nodes.get(val).get('Y'))
            strKML += '%f,%f' %(long,lat)
            strKML += '</coordinates>\n'
            strKML += '</Point>\n'
            strKML += '</Placemark>\n'
    pol_count = 1
    for val in shapes:
        points = val.points
        if (pol_count < 10):
            polygon_element = 'PON_000%d' %pol_count
        elif (pol_count <100):
            polygon_element = 'PON_00%d' %pol_count
        elif (pol_count < 999):
            polygon_element = 'PON_0%d' %pol_count
        else:
            polygon_element = 'PON_%d' %pol_count
        if (nodes.get(polygon_element)):
            depth = nodes.get(polygon_element).get('depth')

            if (depth > 0):
                strKML += '    <Placemark>\n'
                strKML += '      <name>Flood inundation area of reach %s with depth = %f</name>\n'%(polygon_element,depth)
                #polygon_element = 'PON_0001'

                if  (depth >= 0.002) and (depth < 0.05):
                    strKML += '      <styleUrl>#transBluePoly1</styleUrl>\n'
                elif (depth >= 0.05) and (depth <= 0.1):
                    strKML += '      <styleUrl>#transBluePoly2</styleUrl>\n'
                elif (depth >= 0.1) and (depth < 0.2):
                    strKML += '      <styleUrl>#transBluePoly3</styleUrl>\n'
                elif (depth >= 0.2) and (depth < 0.4):
                    strKML += '      <styleUrl>#transBluePoly4</styleUrl>\n'
                elif (depth >= 0.4) and (depth < 0.6):
                    strKML += '      <styleUrl>#transBluePoly5</styleUrl>\n'
                elif (depth >= 0.6) and (depth < 0.8):
                    strKML += '      <styleUrl>#transBluePoly6</styleUrl>\n'
                elif (depth >= 0.8):
                    strKML += '      <styleUrl>#transBluePoly7</styleUrl>\n'

                strKML += '        <Polygon>\n'
                strKML += '        <extrude>0</extrude>\n'
                strKML += '        <altitudeMode>relativeToGround</altitudeMode>\n'
                strKML += '        <outerBoundaryIs>\n'
                strKML += '          <LinearRing>\n'
                strKML += '            <coordinates>\n'
                #strKML += '<description>Depth for %s is %f' %(val,nodes.get(val).get('depth'))
                #strKML += '</description>\n'
                for point in points:
                # The <coordinates> for polygons must be specified in counterclockwise order
                    if DOCOORDTRANFORM:
                        long, lat = pyproj.transform(initEPSG, targetEPSG, point[0],point[1])
                    else: long, lat = point[0],point[1]
                    strKML += '              %f,%f,%d\n'%(long, lat, 10)
        ##        for point in reversed(points):
        ##            if DOCOORDTRANFORM:
        ##                long, lat = pyproj.transform(initEPSG, targetEPSG, point[0],point[1])
        ##            else: long, lat = point[0],point[1]
        ##            strKML += '              %f,%f,%d\n'%(long, lat, 10)
        ##        if DOCOORDTRANFORM:
        ##            long, lat = pyproj.transform(initEPSG, targetEPSG, points[0][0],points[0][1])
        ##        else: long, lat = points[0][0],points[0][1]
                strKML += '              %f,%f,%d\n'%(long, lat,10) # closing the polygon
                strKML += '            </coordinates>\n'
                strKML += '          </LinearRing>\n'
                strKML += '        </outerBoundaryIs>\n'
                strKML += '      </Polygon>\n'
                strKML += '    </Placemark>\n'
        pol_count += 1
    print pol_count
    strKML += '  </Document>\n'
    strKML += '</kml>\n'
    with codecs.open('%s%s'%(HECBASEDIR, KMLFILE), encoding="utf-8", mode="w") as f_kml:
        try:
            f_kml.write(strKML)
        finally:
            f_kml.close()




def create_flow_file(region, startsim, endsim):
  # Create the .fxx file for the given region
    AStr = ''
    AStr = AStr + 'Flow Title=%s\r\n'%region[0]
    AStr = AStr + 'Program Version=4.10\r\n'
    AStr = AStr + 'Number of Profiles= 1\r\n'
    AStr = AStr + 'Profile Names=PF 1\r\n'
    for reach in region[1]:
        distmaxfl = reaches_RAS[reach]
        for idx,dist in enumerate(distmaxfl['maxflows']):
            AStr = AStr + 'River Rch & RM=%s,1               ,%s\r\n'%(reach,dist[0])
            AStr = AStr + '    %f\r\n'%eval(dist[1])

    for reach in region[1]:
        AStr = AStr + 'Boundary for River Rch & Prof#=%s,1               , 1\r\n'%reach
        AStr = AStr + 'Up Type= 3\r\n'
        AStr = AStr + 'Up Slope=%f\r\n'%region[1][reach]['Up Slope']
        AStr = AStr + 'Dn Type= 3\r\n'
        AStr = AStr + 'Dn Slope=%f\r\n'%region[1][reach]['Dn Slope']

    AStr = AStr + 'DSS Import StartDate=%02d%s%04d\r\n'%(startsim.day, monthToStrDSS(startsim.month, True), startsim.year)
    AStr = AStr + 'DSS Import StartTime=00:00\r\n'
    AStr = AStr + 'DSS Import EndDate=%02d%s%04d\r\n'%(endsim.day, monthToStrDSS(endsim.month, True), endsim.year)
    AStr = AStr + 'DSS Import EndTime=00:00\r\n'
    AStr = AStr + 'DSS Import GetInterval= 0\r\n'
    AStr = AStr + 'DSS Import Interval=\r\n'
    AStr = AStr + 'DSS Import GetPeak= 0\r\n'
    AStr = AStr + 'DSS Import FillOption= 0\r\n'

    f_f01 = codecs.open('%s%s%s/%s.f01'%(HECBASEDIR,RASDIR,region[0],region[0]), encoding="utf-8", mode="w")
    try:
        f_f01.write(AStr)
    finally:
        f_f01.close()


def run_ras(region):
    HecRas = win32com.client.Dispatch("RAS41.HECRASController")
    HecRas.Project_Open('%s%s%s/%s.prj'%(HECBASEDIR,RASDIR,region[0],region[0]))
    HecRas.ShowRas()
    #HecRas.Compute_HideComputationWindow()
    HecRas.Compute_CurrentPlan()
    HecRas.ExportGIS()


###print HecRas.CurrentProjectTitle
##  # List of Rivers
##    (nRiver,rivers) = HecRas.Output_GetRivers()
##    (riv, nReaches,reaches) = HecRas.Output_GetReaches(1)
##
##    HecRas.TablePF(rivers[0], reaches[0])
##    HecRas.OutputDSS_GetStageFlow('Spata', '1','9513.137')
##
##    AStr = HecRas.Output_ComputationLevel_Export('comp_spata.txt')
##    f = codecs.open('%s%s%s'%(HECBASEDIR,RASDIR,'comp_spata2.txt'), encoding="utf-8", mode="w")
##    try:
##        f.write(AStr[2])
##    finally:
##        f.close()
##    #const var filename: WideString; const var errmsg: WideString; const var optional WriteFlow: WordBool; const var optional WriteStage: WordBool; const var optional WriteArea: WordBool; const var optional WriteTopWidth: WordBool): Variant; dispid 1610809422;
##
##  # List of Nodes (large number)
##    # riv : River ID
##    # rch : Reach ID
##    # nRS : Number of cross sections
##    # RS  : Names of river stations (array)
##    (riv,rch,nRS,RS,NodeType) = HecRas.Output_GetNodes(1,1)
##  # Get River ID by name
##    # riv = HecRas.Output_GetRiver('Spata')
##
##    #OutputDSS_GetStageFlow(const var River: WideString; const var Reach: WideString; const var RS: WideString; const var nvalue: LongInt; const var ValueDateTime: array of Double; const var Stage: array of Single; const var Flow: array of Single; const var errmsg: WideString): WordBool; dispid 1610809424;
##
##
##
##    #print HecRas.Compute_IsStillComputing()
##    #HecRas.Compute_CurrentPlan(1, mcomp)
##
##    #For i = 1 To idCS.GetUpperBound(0)
##    #ioFile.WriteLine(hecras.Output_NodeOutput(1, 1, i, 1, 1, 208)) 'Sometimes works and sometimes does not
##    #Next

def createKMLsimple(nodes,shapes,scenario,startsim):
      # Do the coordinates transformation
    if DOCOORDTRANFORM:
        initEPSG   = pyproj.Proj("+init=EPSG:%s"%INITEPSG)
        targetEPSG = pyproj.Proj("+init=EPSG:%s"%TARGETEPSG)
    kml = simplekml.Kml()
    for reach in reaches_RAS:
        outerbound = []
        for cs in reaches_RAS[reach]['flood_area']:
            if DOCOORDTRANFORM:
                long, lat = pyproj.transform(initEPSG, targetEPSG, cs[1][0],cs[1][1])
            else: long, lat = cs[1][0],cs[1][1]
            outerbound.append((long, lat))
        for cs in reversed(reaches_RAS[reach]['flood_area']):
            if DOCOORDTRANFORM:
                long, lat = pyproj.transform(initEPSG, targetEPSG, cs[0][0],cs[0][1])
            else: long, lat = cs[0][0],cs[0][1]
            outerbound.append((long, lat))
        if DOCOORDTRANFORM:
            long, lat = pyproj.transform(initEPSG, targetEPSG, reaches_RAS[reach]['flood_area'][0][1][0],reaches_RAS[reach]['flood_area'][0][1][1])
        else: long, lat = reaches_RAS[reach]['flood_area'][0][1][0],reaches_RAS[reach]['flood_area'][0][1][1]
        outerbound.append((long, lat,10))
        pol = kml.newpolygon(name=reach,outerboundaryis = outerbound)
        pol.style.polystyle.color = '7dff0000'
        pol.style.linestyle.color = '7dff0000'
        pol.style.linestyle.width= 2

    #swmm output
    style1 = simplekml.Style()
    style1.polystyle.color = 'ff14E7FF'
    style1.linestyle.color = 'ff14E7FF'
    style1.linestyle.width = 2
    style2 = simplekml.Style()
    style2.polystyle.color = 'ff14CCFF'
    style2.linestyle.color = 'ff14CCFF'
    style2.linestyle.width = 2
    style3 = simplekml.Style()
    style3.polystyle.color = 'ff14C3FF'
    style3.linestyle.color = 'ff14C3FF'
    style3.linestyle.width = 2
    style4 = simplekml.Style()
    style4.polystyle.color = 'ff14B1FF'
    style4.linestyle.color = 'ff14B1FF'
    style4.linestyle.width = 2
    style5 = simplekml.Style()
    style5.polystyle.color = 'ff14A0FF'
    style5.linestyle.color = 'ff14A0FF'
    style5.linestyle.width = 2
    style6 = simplekml.Style()
    style6.polystyle.color = 'ff1485FF'
    style6.linestyle.color = 'ff1485FF'
    style6.linestyle.width = 2
    style7 = simplekml.Style()
    style7.polystyle.color = 'ff1447FF'
    style7.linestyle.color = 'ff1447FF'
    style7.linestyle.width = 2

    pol_count = 1
    for val in shapes:
        points = val.points
        if (pol_count < 10):
            polygon_element = 'PON_000%d' %pol_count
        elif (pol_count <100):
            polygon_element = 'PON_00%d' %pol_count
        elif (pol_count < 999):
            polygon_element = 'PON_0%d' %pol_count
        else:
            polygon_element = 'PON_%d' %pol_count
        if (nodes.get(polygon_element)):
            depth = nodes.get(polygon_element).get('depth')
            outerbound = []
            if (depth > 0):
                for point in points:
                # The <coordinates> for polygons must be specified in counterclockwise order
                    if DOCOORDTRANFORM:
                        long, lat = pyproj.transform(initEPSG, targetEPSG, point[0],point[1])
                    else: long, lat = point[0],point[1]
                    outerbound.append((long, lat,10))
                pol = kml.newpolygon(name='Flood inundation area of reach %s with depth = %f</name>\n'%(polygon_element,depth),outerboundaryis = outerbound)
                if  (depth >= 0.002) and (depth < 0.05):
                    pol.style = style1
                elif (depth >= 0.05) and (depth <= 0.1):
                    pol.style = style2
                elif (depth >= 0.1) and (depth < 0.2):
                    pol.style = style3
                elif (depth >= 0.2) and (depth < 0.4):
                    pol.style = style4
                elif (depth >= 0.4) and (depth < 0.6):
                    pol.style = style5
                elif (depth >= 0.6) and (depth < 0.8):
                    pol.style = style6
                elif (depth >= 0.8):
                    pol.style = style7
        pol_count += 1
        KMLFILE2 = ''
    if(scenario == 0):
        KMLFILE2 = '%d_%d_%d_forecast' %(startsim.day,startsim.month,startsim.year)
    elif (scenario == 1):
        KMLFILE2 = '%d_%d_%d_dry' %(startsim.day,startsim.month,startsim.year)
    elif (scenario == 2):
        KMLFILE2 = '%d_%d_%d_wet' %(startsim.day,startsim.month,startsim.year)
    kml.savekmz("%s_%s.kmz"%(KMLFILE,KMLFILE2))  # Saving as KMZ

##    host = "192.168.1.100"
##
##    port = 22
##
##    transport = paramiko.Transport((host,port))
##
##    password = "zach0sal3xandr0s"
##
##    username = "azachos"
##
##    transport.connect(username=username,password=password)
##
##    sftp = paramiko.SFTPClient.from_transport(transport)
##
##    sftp.put('%sscripts/%s_%s.kmz'%(HECBASEDIR, KMLFILE,KMLFILE2),"/var/www/html/flire/%s_%s.kmz"%(KMLFILE,KMLFILE2))
##
##    sftp.close()
##
##    transport.close()
